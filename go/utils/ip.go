package utils

import (
	"net/http"
	"strings"
)

func GetIp(r *http.Request) string {

	if !strings.Contains(r.RemoteAddr, "127.0.0.1") {
		return r.RemoteAddr
	}

	if r.Header.Get("X-Real-IP") != "" {
		return r.Header.Get("X-Real-IP")
	}

	return "0.0.0.0:69420"
}
