package activityLogger_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/kolioDev/cv/activityLogger"
	"github.com/stretchr/testify/assert"
)

const REMOTE_ADDRESS = "123.456.789.101"
const LOG_FILE = "../activity.log"
const DATE_FORMAT = "Mon Jan 2 15:04:05 2006"

func TestLogActivity(t *testing.T) {
	os.Setenv("ACTIVITY_LOG_FILE", LOG_FILE)

	// Log the activity in the list
	// Remove first log if the number of logs exceed max
	params := url.Values{}
	params.Add("browser", "chrome")
	params.Add("os", "Linux")
	params.Add("activity", "techStackOpen")

	// Successful request - all params present
	rr := makeRequest(params)
	assert.Equalf(t, 200, rr.Code, "Received code %d with body (%s)", rr.Code, rr.Body.String())
	//Read the contents of the log file
	logFile, err := os.ReadFile(LOG_FILE)
	assert.NoError(t, err)
	logLines := strings.Split(string(logFile), "\n")
	lastLine := logLines[len(logLines)-2]
	expectedLog := fmt.Sprintf("%s ||             techStackOpen || %s ||                                                     \"chrome\" ||              \"Linux\"", time.Now().UTC().Format(DATE_FORMAT), REMOTE_ADDRESS)
	assert.Equal(t, expectedLog, lastLine)
	logLineParams := strings.Split(lastLine, "||")
	assert.Equal(t, REMOTE_ADDRESS, strings.TrimSpace(logLineParams[2]))
	assert.Equal(t, "techStackOpen", strings.TrimSpace(logLineParams[1]))
	assert.Equal(t, time.Now().UTC().Format(DATE_FORMAT), strings.TrimSpace(logLineParams[0]))
	assert.Equal(t, "\"chrome\"", strings.TrimSpace(logLineParams[3]))
	assert.Equal(t, "\"Linux\"", strings.TrimSpace(logLineParams[4]))

	// Successful request - no activity param present
	params.Del("activity")
	rr = makeRequest(params)
	assert.Equalf(t, 200, rr.Code, "Received code %d with body (%s)", rr.Code, rr.Body.String())
	//Read the contents of the log file
	logFile, err = os.ReadFile(LOG_FILE)
	assert.NoError(t, err)
	logLines = strings.Split(string(logFile), "\n")
	lastLine = logLines[len(logLines)-2]
	logLineParams = strings.Split(lastLine, "||")
	assert.Equal(t, REMOTE_ADDRESS, strings.TrimSpace(logLineParams[2]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[1]))
	assert.Equal(t, time.Now().UTC().Format(DATE_FORMAT), strings.TrimSpace(logLineParams[0]))
	assert.Equal(t, "\"chrome\"", strings.TrimSpace(logLineParams[3]))
	assert.Equal(t, "\"Linux\"", strings.TrimSpace(logLineParams[4]))

	// Successful request - no browser param present
	params.Del("browser")
	rr = makeRequest(params)
	assert.Equalf(t, 200, rr.Code, "Received code %d with body (%s)", rr.Code, rr.Body.String())
	//Read the contents of the log file
	logFile, err = os.ReadFile(LOG_FILE)
	assert.NoError(t, err)
	logLines = strings.Split(string(logFile), "\n")
	lastLine = logLines[len(logLines)-2]
	logLineParams = strings.Split(lastLine, "||")
	assert.Equal(t, REMOTE_ADDRESS, strings.TrimSpace(logLineParams[2]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[1]))
	assert.Equal(t, time.Now().UTC().Format(DATE_FORMAT), strings.TrimSpace(logLineParams[0]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[3]))
	assert.Equal(t, "\"Linux\"", strings.TrimSpace(logLineParams[4]))

	// Successful request - no browser and os params present
	params.Del("os")
	rr = makeRequest(params)
	assert.Equalf(t, 200, rr.Code, "Received code %d with body (%s)", rr.Code, rr.Body.String())
	//Read the contents of the log file
	logFile, err = os.ReadFile(LOG_FILE)
	assert.NoError(t, err)
	logLines = strings.Split(string(logFile), "\n")
	lastLine = logLines[len(logLines)-2]
	logLineParams = strings.Split(lastLine, "||")
	assert.Equal(t, REMOTE_ADDRESS, strings.TrimSpace(logLineParams[2]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[1]))
	assert.Equal(t, time.Now().UTC().Format(DATE_FORMAT), strings.TrimSpace(logLineParams[0]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[3]))
	assert.Equal(t, "[unknown]", strings.TrimSpace(logLineParams[4]))

	// Try to overflow the file
	for i := 0; i < 10010; i++ {
		rr = makeRequest(params)
		assert.Equalf(t, 200, rr.Code, "Received code %d with body (%s)", rr.Code, rr.Body.String())
		logFile, err = os.ReadFile(LOG_FILE)
		assert.NoError(t, err)
		logLines = strings.Split(string(logFile), "\n")
		assert.Less(t, len(logLines), 10002)
	}
	// for l := 0; l < len(logLines); l++ {
	// 	logLineParams := strings.Split(logLines[l], "||")
	// 	assert.Equal(t, strings.TrimSpace(logLineParams[1]), "[unknown]")
	// 	assert.Equal(t, strings.TrimSpace(logLineParams[3]), "[unknown]")
	// 	assert.Equal(t, strings.TrimSpace(logLineParams[4]), "[unknown]")
	// }

}

func makeRequest(params url.Values) *httptest.ResponseRecorder {
	body := params.Encode()

	req, _ := http.NewRequest("POST", "/activity/log", strings.NewReader(body))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.RemoteAddr = REMOTE_ADDRESS
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(activityLogger.LogActivity)
	handler.ServeHTTP(rr, req)

	return rr
}
