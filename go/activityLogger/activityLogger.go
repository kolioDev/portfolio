package activityLogger

import (
	"bufio"
	"fmt"
	"html"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/kolioDev/cv/utils"
)

const MAX_NUMBER_OF_LOGS = 10000
const LOG_LINE_DIVIDER = "\n"

const UNKNOWN_PLACEHOLDER = "[unknown]"

type Activity int

type User struct {
	Date     time.Time
	IP       string
	Browser  string
	OS       string
	Activity Activity
}

var ACTIVITY_TYPES = []string{
	"[unknown]", //when the activity sent does not correspond to anything known

	"visit",     //when the user opens the website
	"arrowDown", //when the user uses the arrow down

	"navAbout",               //when the user uses the navigation button "who am I"
	"navBackground",          //when the user uses the navigation button "background"
	"navTechs",               //when the user uses the navigation button "technologies"
	"navEngineeringProjects", // when the user uses the navigation button "engineering projects"
	"navSoftwareProjects",    // when the user uses the navigation button "software projects"
	"navHire",                //when the user uses the navigation button "contact me"

	"bannerHireMeTodayButton", //when they click on the hire me today button in the banner

	"aboutMore",   //when the find out more is pressed on the about page
	"aboutCity",   //when they cycle on the different city pictures
	"aboutHobies", //when the cycle on the different hobbies on the about page

	"timelineButtonPrices",    //when the modal with the грамоти is opened
	"timelineButtonTa",        //when the modal with the ta jobs is opened
	"timelineButtonCourses",   //when the modal with the university courses is opened
	"timelinePricesChangeTab", // when the user navigates trough the different tabs in the грамоти modal
	"timelineTAJobDetails",    // when the user gets more details for a job
	"timelineCourseDetails",   // when the user gets more datails for a course
	"timelineCoursesSort",     // when the user uses the sort mechanism

	"3dStoryOpen",     //when the button "yes show me the 3D story is clicked"
	"strSliderUsed",   //when they first use the slider in the 3d story
	"strFinished",     //when they finish the "essential" part of the 3d story
	"strFinishedFnl",  //when the completely finish the story
	"storyMovedFrw",   //when they move forward in the 3d story
	"story3dReCenter", //when the automatic recenter is pressed
	"goto2D",          //when story go to 2d is pressed
	"storySound",      //when the sound the 3d story is adjusted
	"storyCC",         //when the CC button in the 3d story is pressed

	"techExpand", // when any of the techs is expanded on the techs section

	"techStackOpen",             //when any stack of technologies is opened in the project section
	"engineeringSkillStackOpen", //when the user clicks the button "show the skills required" on the engineering project page
	"projectFollowUrl",          //when they click on any of the project urls

	"linkedinOpen",          //when the link to my linked in the contact section is clicked
	"cvDownload",            //when someone downloads the CV
	"formTypeEmail",         //when they start typying their email in the form
	"formTypeMessage",       //when they start typing a message in the form
	"formSendMessageButton", //when they click the button for sending the message
	"formMessageSuccess",    //when the message was sent successfully
	"formMessageError",      //message not sent successfully

	"tajobsOpenedFromALink",  // when the ta jobs modal is opened from a http://localhost:5000/?modal=tajobs/#story link
	"coursesOpenedFromALink", // when the ta jobs modal is opened from a http://localhost:5000/?modal=courses/#story link
	"pricesOpenedFromALink",  // when the ta jobs modal is opened from a http://localhost:5000/?modal=prices/#story link

}

func (u User) String() string {
	f := "Mon Jan 2 15:04:05 2006"

	getDefValue := func(s string) string {
		if s == "" || s == "\"\"" {
			return UNKNOWN_PLACEHOLDER
		}
		return s
	}

	return fmt.Sprintf("%s || %25s || %15s || %60s || %20s", u.Date.Format(f), ACTIVITY_TYPES[u.Activity], getDefValue(u.IP), getDefValue(u.Browser), getDefValue(u.OS))
}

func LogActivity(w http.ResponseWriter, r *http.Request) {
	var LOG_FILE = os.Getenv("ACTIVITY_LOG_FILE")
	if LOG_FILE == "" {
		LOG_FILE = "./activity.log"
	}

	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method Not Allowed"))
		return
	}

	r.ParseForm()
	browser := r.PostForm.Get("browser")
	user_os := string(html.EscapeString(r.PostForm.Get("os")))
	activity := r.PostForm.Get("activity")
	activity_id := 0

	for i := 0; i < len(ACTIVITY_TYPES); i++ {
		if ACTIVITY_TYPES[i] == activity {
			activity_id = i
			break
		}
	}

	u := User{
		Date:     time.Now().UTC(),
		IP:       utils.GetIp(r),
		Browser:  strconv.Quote(browser),
		OS:       strconv.Quote(user_os),
		Activity: Activity(activity_id),
	}

	// Open the file in append mode, create it if it doesn't exist, with write-only permissions
	file, err := os.OpenFile(LOG_FILE, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Cannot open log file"))
	}
	// Write the content to the file
	if _, err := file.WriteString(fmt.Sprintf("%s\n", u.String())); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to write to file"))
	}
	file.Close()

	file, _ = os.Open(LOG_FILE)
	scanner := bufio.NewScanner(file)
	lineCount := 0
	for scanner.Scan() {
		lineCount++
	}
	file.Close()

	// Check if the file has more than 1000 lines
	if lineCount > MAX_NUMBER_OF_LOGS {
		// Make a copy of the file
		file, _ = os.Open(LOG_FILE)
		var backupFilePath = fmt.Sprintf("%s_back_%s", LOG_FILE, time.Now().Format("02-01-2006"))

		destinationFile, err := os.Create(backupFilePath)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Cannot copy big old file"))
			return
		}
		defer destinationFile.Close()

		_, err = io.Copy(destinationFile, file)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Cannot copy big old file"))
			return
		}
		destinationFile.Sync()

		overwriteFile(LOG_FILE, fmt.Sprintf("File backed up to %s\n", backupFilePath))
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Saved"))
}

// overwriteFile overwrites the content of the specified file
func overwriteFile(filePath, content string) error {
	// Open the file with write-only mode and truncate it
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write the new content to the file
	_, err = file.WriteString(content)
	return err
}
