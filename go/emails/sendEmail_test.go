package emails_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/kolioDev/cv/emails"
	"github.com/stretchr/testify/assert"
)

func TestSendMail(t *testing.T) {
	go emails.ClearIPs()

	params := url.Values{}
	params.Add("message", "Това съобщение е пратено заради go test-ването на emails.SendMail")
	params.Add("email", "test@gmail.com")

	rr := makeRequest(params)
	assert.Equal(t, 200, rr.Code)

	rr = makeRequest(params)
	assert.Equal(t, 429, rr.Code)

	time.Sleep(2 * time.Minute)
	params.Del("message")
	rr = makeRequest(params)
	assert.Equal(t, 406, rr.Code)
	params.Add("message", "Това съобщение е пратено заради go test-ването на emails.SendMail")

	time.Sleep(2 * time.Minute)
	params.Del("email")
	rr = makeRequest(params)
	assert.Equal(t, 406, rr.Code)
	params.Add("email", "test@gmail.com")

	time.Sleep(2 * time.Minute)
	key := os.Getenv("MAILGUN_KEY")
	os.Setenv("MAILGUN_KEY", "nope")
	rr = makeRequest(params)
	assert.Equal(t, 500, rr.Code)
	os.Setenv("MAILGUN_KEY", key)
}

func makeRequest(params url.Values) *httptest.ResponseRecorder {
	body := params.Encode()

	req, _ := http.NewRequest("POST", "/send/email", strings.NewReader(body))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(emails.SendMail)
	handler.ServeHTTP(rr, req)

	return rr
}

//yGZ@&Z-bPg2r?NPF
