package emails

import (
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/kolioDev/cv/utils"
	"github.com/mailgun/mailgun-go"
)

var ips []string

var domain = os.Getenv("MAILGUN_DOMAIN") //"mail.koliodev.com"
var privateAPIKey = os.Getenv("MAILGUN_KEY")

type email string
type message string

// Validate валидация на email-a
func (e email) Validate() bool {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if !re.MatchString(string(e)) || len(e) == 0 || len(e) > 512 {
		return false
	}
	return true
}

// Validate валидация на съобщението
func (m message) Validate() bool {
	if len(m) > 5 && len(m) < 4048 {
		return true
	}
	return false
}

// SendMail праща ми E-mail
func SendMail(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method Not Allowed"))
		return
	}

	if contains(ips, utils.GetIp(r)) {
		w.WriteHeader(http.StatusTooManyRequests)
		w.Write([]byte("Too many emails send. Please wait"))
		return
	}

	if r.Method == "POST" {
		log.Println("making email post request")
		logIP(r)
		r.ParseForm()

		var mail = email(html.EscapeString(r.PostForm.Get("email")))
		var msg = message(html.EscapeString(r.PostForm.Get("message")))
		if !mail.Validate() || !msg.Validate() {
			w.WriteHeader(http.StatusNotAcceptable)
			return
		}

		// Create an instance of the Mailgun Client
		mg := mailgun.NewMailgun(domain, privateAPIKey)
		mg.SetAPIBase("https://api.eu.mailgun.net/v3")

		sender := "support@koliodev.com"
		subject := "Съобщение от потребител"
		body := fmt.Sprintf(`
		Oт: %s
		Съобщение: %s
		 `, mail, msg)
		recipient := "koliodev@protonmail.com"

		// The message object allows you to add attachments and Bcc recipients
		message := mg.NewMessage(sender, subject, body, recipient)

		// Send the message	with a 10 second timeout
		resp, id, err := mg.Send(message)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err.Error())
			return
		}

		log.Printf("ID: %s Resp: %s\n", id, resp)
		w.Write([]byte("ok"))
	}
}

// logIP записва дадено ip в масива
func logIP(r *http.Request) {
	if !contains(ips, utils.GetIp(r)) {
		ips = append(ips, utils.GetIp(r))
		log.Println(ips)
	}
}

// contains проверява, дали  strings  се садържа в масив от string-ове
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// ClearIPs Изчиства таблицата* с ip-тата на всеки 2 минути
func ClearIPs() {

	for {
		time.Sleep(2 * time.Minute)
		if len(ips) > 0 {
			log.Println("clearing ips table ", ips)
			ips = []string{}
		}

	}
}
