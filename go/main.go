package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/kolioDev/cv/activityLogger"
	"github.com/kolioDev/cv/emails"
)

func main() {

	http.HandleFunc("/send/email", emails.SendMail)
	http.HandleFunc("/activity/log", activityLogger.LogActivity)

	fs := http.FileServer(http.Dir("../public"))
	http.Handle("/", http.StripPrefix("/", fs))

	go emails.ClearIPs()

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	log.Printf("Listening on port %s ...\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		log.Fatal(err)
	}
}
