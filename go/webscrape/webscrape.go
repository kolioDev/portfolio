// Command text is a chromedp example demonstrating how to extract text from a
// specific element.
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"time"

	"github.com/chromedp/chromedp"
)

const HEADLESS = false
const OSIRIS_URL = "https://tue.osiris-student.nl/"
const GRADES_URL = OSIRIS_URL + "#/voortgang/:id"
const OPEN_PASSED_GRADES_BTN = ".material-icons.ng-tns-c0-5"
const OPEN_FAILED_GRADES_BTN = ".ng-tns-c0-3.ng-star-inserted"

const GRADES_SELECTOR = "ion-col>osi-li-body.osi-black-38.padding-l .font-li-body.osi-black-87.font-size-"

const JS = `Array.from(document.querySelectorAll(".font-li-body.osi-black-87.font-size-")).filter(e=>e.innerText=='5 EC').length`

const INDEX_HTML_PATH = "../../public/index.html"
const INDEX_HTML_OPEN_COMMENT = `<!--Webscraped data\[START\]-->`
const INDEX_HTML_CLOSE_COMMENT = `<!--Webscraped data\[END\]-->`

type GradesData struct {
	Passed int `json:"passed"`
	Failed int `json:"failed"`
}

func main() {
	USERNAME := os.Getenv("OSIRIS_USERNAME")
	PASSWORD := os.Getenv("OSIRIS_PASSWORD")

	gd, err := scrape(USERNAME, PASSWORD)
	if err != nil {
		errorHandl(err)
		return
	}
	err = updateIndexHtml(gd)
	if err != nil {
		errorHandl(err)
		return
	}

	successStr := fmt.Sprintf("%v", time.Now())
	ioutil.WriteFile("success.log", []byte(successStr), 0644)
}

func errorHandl(err error) {
	log.Fatal(err)
	ioutil.WriteFile("error.log", []byte(err.Error()), 0644)

}

func updateIndexHtml(gd GradesData) error {
	s, err := json.Marshal(gd)
	if err != nil {
		return err
	}
	replStr := fmt.Sprintf(`
	%s 
	<script>
		window.webscrapedData=%s
	</script>
	%s
	`, INDEX_HTML_OPEN_COMMENT, string(s), INDEX_HTML_CLOSE_COMMENT)

	indexHtml, err := ioutil.ReadFile(INDEX_HTML_PATH)
	if err != nil {
		return err
	}

	re := regexp.MustCompile(fmt.Sprintf(`%s.*\s*%s`, INDEX_HTML_OPEN_COMMENT, INDEX_HTML_CLOSE_COMMENT))
	indexHtml = []byte(re.ReplaceAllString(string(indexHtml), replStr))

	ioutil.WriteFile(INDEX_HTML_PATH, []byte(indexHtml), 0644)

	return nil
}

func scrape(uname, pass string) (GradesData, error) {
	//Set headless flag
	allocatorContext, cancel := chromedp.NewExecAllocator(context.Background(), chromedp.Flag("headless", HEADLESS))
	defer cancel()

	// create context
	ctx, cancel := chromedp.NewContext(allocatorContext)
	defer cancel()

	// run task list
	gd := GradesData{}
	err := chromedp.Run(ctx,
		// Navigate to Osiris
		chromedp.Navigate(OSIRIS_URL),
		// Login
		chromedp.WaitVisible("#formsAuthenticationArea", chromedp.ByID),
		info("Navigated to login"),
		chromedp.SendKeys(`input[name=UserName]`, uname),
		chromedp.SendKeys(`input[name=Password]`, pass),
		chromedp.Click("#submitButton", chromedp.ByID),
		//Navigate to progress page
		chromedp.WaitReady("osi-home-shortcut-item"),
		chromedp.Navigate(GRADES_URL),
		chromedp.WaitVisible(OPEN_PASSED_GRADES_BTN),
		// Open passed courses tab
		chromedp.Click(OPEN_PASSED_GRADES_BTN, chromedp.ByQuery),
		chromedp.WaitVisible(".font-li-body.osi-black-87.font-size-"),
		chromedp.Evaluate(JS, &gd.Passed),
		chromedp.Sleep(time.Second*1),

		//Open failed courses tab
		chromedp.Click(OPEN_PASSED_GRADES_BTN, chromedp.ByQuery),
		chromedp.Sleep(time.Second*1),
		chromedp.Click(OPEN_FAILED_GRADES_BTN, chromedp.ByQuery),
		chromedp.Evaluate(JS, &gd.Failed),
	)
	if err != nil {
		return gd, err
	}

	return gd, nil
}

func info(m string) chromedp.Action {
	return chromedp.ActionFunc(func(c context.Context) error {
		fmt.Printf("%s\n", m)
		return nil
	})
}
