import { locale, dictionary } from 'svelte-i18n'
import bg from './dicts/bg';
import en from './dicts/en';

dictionary.subscribe(v => {
  locale.subscribe(l => {
    // window.i18n = v[l];
  });
});

// Define a locale dictionary
dictionary.set({
  bg, en
})



