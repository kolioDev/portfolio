import nav from './bg/nav.json';
import banner from './bg/banner.json';
import about from './bg/about.json';
import techs from './bg/techs.json';
import softwareProjects from  './bg/softwareProjects.json';
import engineeringProjects from './bg/engineeringProjects.json';
import hire from './bg/hire.json';
import foot from './bg/foot.json';
import general from './bg/general.json';

export default {
  nav,
  banner,
  about,
  techs,
  softwareProjects,
  engineeringProjects,
  hire,
  foot,
  general
}