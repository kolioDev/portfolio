import nav from './en/nav.json';
import banner from './en/banner.json';
import about from './en/about.json';
import techs from './en/techs.json';
import softwareProjects from  './en/softwareProjects.json';
import engineeringProjects from './en/engineeringProjects.json';
import hire from './en/hire.json';
import foot from './en/foot.json';
import story from './en/story.json';
import storyShort from './en/storyShort.json';
import diplomaList from './en/diplomaList.json';
import general from './en/general.json';
import unicourses from './en/unicourses.json';
import taJobs from './en/taJobs.json';

export default {
  nav,
  banner,
  about,
  techs,
  softwareProjects,
  engineeringProjects,
  hire,
  foot,
  story,
  storyShort,
  diplomaList,
  general,
  unicourses,
  taJobs
}