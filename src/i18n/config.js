import { init, getLocaleFromQueryString } from 'svelte-i18n'


init({
  fallbackLocale: 'en',
  initialLocale: getLocaleFromQueryString('locale'),
});