export const getDiplomaList = (i18n) => ({
    "physics": {
        title: i18n("diplomaList.physics.title"),
        icon: "fas fa-atom",
        diplomas: [
            {
                year: 2020,
                text: i18n("diplomaList.physics.2020Regional"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.physics.2019National"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.physics.2019Regional"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.physics.2019Autumn"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.physics.2019AutumnParticipation"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.physics.2018Regional"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.physics.2018Autumn"),
            },

            {
                year: 2017,
                text: i18n("diplomaList.physics.2017National"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.physics.2017Spring"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.physics.2017Reginal"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.physics.2016Autumn"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.physics.2016AutumnParticipation"),
            },
        ]
    },
    "it": {
        title: i18n("diplomaList.it.title"),
        shortTitle: i18n("diplomaList.it.shortTitle"),
        icon: 'fas fa-laptop',
        diplomas: [
            {
                year: 2020,
                text: i18n("diplomaList.it.2020National"),
            },
            {
                year: 2020,
                text: i18n("diplomaList.it.2020NationalParticipation"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.it.2019NationalParticipation"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.it.2019Regional"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.it.2018NationalGreatPerformance"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.it.2018NationalParticipation"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.it.2018Regional"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.it.2017NationalGreatPerformance"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.it.2017NationalParticipation"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.it.2017Regional"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.it.2017VFU"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.it.2016NationalParticipation"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.it.2016John"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.it.2016Regional"),
            },
            {
                year: 2016,
                text: i18n("diplomaList.it.2016WebLoz"),
            },
        ]
    },
    "others": {
        title: i18n("diplomaList.others.title"),
        icon: 'fas fa-random',
        diplomas: [
            {
                year: 2020,
                text: i18n("diplomaList.others.telebid2020"),
            },
            {
                year: 2020,
                text: i18n("diplomaList.others.bmx2020"),
            },
            {
                year: "2017-2020",
                text: i18n("diplomaList.others.scholarshipEG2017-2020"),
            },
            {
                year: 2019,
                text: i18n("diplomaList.others.telebid2019"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.others.scholarshipOneTimeEG2018"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.others.softuni2018"),
            },
            {
                year: 2018,
                text: i18n("diplomaList.others.translate2018"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.others.creativeWriting2017"),
            },
            {
                year: 2017,
                text: i18n("diplomaList.others.bmx2017"),
            }
        ],
    }
})
