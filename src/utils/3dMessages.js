import _ from 'lodash';

import Welcome from "../Components/Story/Story3d/Messages/Welcome.svelte";
import InstructionsMessage from "../Components/Story/Story3d/Messages/InstructionsMessage.svelte";
import ImageMessage from '../Components/Story/Story3d/Messages/ImageMessage.svelte';
import TextMessage from '../Components/Story/Story3d/Messages/TextMessage.svelte';
import TravelMessage from '../Components/Story/Story3d/Messages/TravelMessage.svelte';
import FinishMessage from '../Components/Story/Story3d/Messages/FinishMessage.svelte';

import { LAYOUT_ITEMS } from "../3d/Layout/layoutItems";
import { LAYOUT_ITEMS_PROPS } from '../3d/Layout/layoutItemsProps';
import { yearsSince } from "./time";

export const WELCOME_KEY = "WELCOME";
export const INSTRUCTIONS_KEY = "INSTRUCTIONS";

const INSTRUCTION_MEDIA_PATH = '/story/instructions/';

const SHORT = "500px";
const WIDE = "700px";
const WIDER = "850px";
const WIDEST = "950px";

const messageTypes = {
    welcome: {
        component: Welcome,
        dismissOnSpace: false,
        stopAnimation: false,
        boxWidth: SHORT,
    },

    instructions: {
        component: InstructionsMessage,
        dismissOnSpace: false,
        stopAnimation: true,
        boxWidth: WIDE,
    },

    image: {
        component: ImageMessage,
        dismissOnSpace: true,
        stopAnimation: true,
        boxWidth: WIDE,
        // text,
        // image,
        // title,
        // imageAlt,
    },

    text: {
        component: TextMessage,
        dismissOnSpace: true,
        stopAnimation: true,
        // text,
        // title,
        // bullets,
        // boxWidth,
    }
}

const simpleStoryMessage = (key, i18n, i18nJson) => ({
    ...messageTypes.text,
    text: i18n(`story.messages.${key}`),
    title: i18n(`story.titles.${key}`),
    bullets: i18nJson(`story.bullets.${key}`),
    boxWidth: i18n(`story.messages.${key}`).length > 900 ? WIDER : WIDE
})

export const getMessages = (i18n, i18nJson) => {

    const messages = {
        [WELCOME_KEY]: messageTypes.welcome,
        [INSTRUCTIONS_KEY]: {
            ...messageTypes.instructions,
            instructions:[
                {
                    text: i18n(`story.instructions.objectClicking`),
                    video:  INSTRUCTION_MEDIA_PATH+'object_clicking.mp4',
                    poster:  INSTRUCTION_MEDIA_PATH+'object_clicking_poster.png',
                },
                {
                    text: i18n(`story.instructions.stopSign`),
                    video:  INSTRUCTION_MEDIA_PATH+'stop_sign.mp4',
                    poster:  INSTRUCTION_MEDIA_PATH+'stop_sign_poster.png',
                },
                {
                    text: i18n(`story.instructions.techUnlock`),
                    video:  INSTRUCTION_MEDIA_PATH+'tech_unlock.mp4',
                    poster:  INSTRUCTION_MEDIA_PATH+'tech_unlock_poster.png',
                },
                {
                    text: i18n(`story.instructions.slider`),
                    video:  INSTRUCTION_MEDIA_PATH+'slider.mp4',
                    poster:  INSTRUCTION_MEDIA_PATH+'slider_poster.png',
                },
                {
                    text: i18n(`story.instructions.volume`),
                    video:  INSTRUCTION_MEDIA_PATH+'volume_setting.mp4',
                    poster:  INSTRUCTION_MEDIA_PATH+'volume_setting_poster.png',
                }
            ]
        },

        [LAYOUT_ITEMS.pmg]: simpleStoryMessage('PMG', i18n, i18nJson),
        [LAYOUT_ITEMS.eg]: simpleStoryMessage('EG', i18n, i18nJson),
        [LAYOUT_ITEMS.webLoz]: simpleStoryMessage('webloz', i18n, i18nJson),
        [LAYOUT_ITEMS.pe6o]: simpleStoryMessage('pe6o', i18n, i18nJson),
        [LAYOUT_ITEMS.physics]: simpleStoryMessage('physics', i18n, i18nJson),
        [LAYOUT_ITEMS.referee]: simpleStoryMessage('referee', i18n, i18nJson),
        [LAYOUT_ITEMS.uchase]: simpleStoryMessage('uchase', i18n, i18nJson),
        [LAYOUT_ITEMS.graduation]: simpleStoryMessage('graduation', i18n, i18nJson),
        [LAYOUT_ITEMS.uchaseServer]: {
            ...messageTypes.text,
            text: i18n(`story.messages.uchaseCoolStuf`),
            title: i18n(`story.titles.uchaseCoolStuf`),
            boxWidth: WIDER,
        },
        [LAYOUT_ITEMS.tue]: {
            ...messageTypes.text,
            text: i18n(`story.messages.tue`, {
                values: {
                    completed_courses: webscrapedData.passed,
                    failed_courses: webscrapedData.failed,
                    credits: webscrapedData.passed * 5,
                    total_credits: 180,
                }
            }),
            title: i18n(`story.titles.tue`),
            bullets: i18nJson(`story.bullets.tue`),
            boxWidth: WIDER,
        },

        [LAYOUT_ITEMS.finish]: {
            component: FinishMessage,
            dismissOnSpace: true,
            stopAnimation: true,
            mainText: i18n(`story.messages.finish.main`),
            gotoNextSectionText: i18n('story.messages.finish.gotoNextSection'),
            title: i18n("story.titles.finish"),
            boxWidth: SHORT,
        },

        [LAYOUT_ITEMS.mountainBike]: {
            ...messageTypes.image,
            text: i18n("story.messages.mountainBike", {
                values: {
                    years: yearsSince("2014-01-01T01:00:00")
                }
            }),
            image: "/story/e3.jpg",
            title: i18n("story.titles.mountainBike"),
            boxWidth: WIDER,
            imageAlt: i18n("story.mediaAlt.mountainBike"),
        },

        [LAYOUT_ITEMS.bmxStatic]: {
            ...messageTypes.image,
            text: i18n("story.messages.bmx"),
            image: "/story/bmx.jpg",
            title: i18n("story.titles.bmx"),
            boxWidth: WIDEST,
            imageAlt: i18n("story.mediaAlt.bmx"),
        },

        [LAYOUT_ITEMS.train]: {
            component: TravelMessage,
            dismissOnSpace: true,
            stopAnimation: true,
            boxWidth: WIDEST,
            title: i18n(`story.titles.travel`),
        },

        [LAYOUT_ITEMS.mountain]: {
            ...messageTypes.image,
            text: i18n("story.messages.mountain"),
            image: "/story/mountain.jpg",
            title: i18n("story.titles.mountain"),
            boxWidth: WIDER,
            imageAlt: i18n("story.mediaAlt.mountain"),
        },

        [LAYOUT_ITEMS.paraglider]:
        {
            ...messageTypes.image,
            text: i18n("story.messages.paraglider"),
            image: "/story/paraglider.jpg",
            title: i18n("story.titles.paraglider"),
            boxWidth: WIDER,
            imageAlt: i18n("story.mediaAlt.paraglider"),
        },

        [LAYOUT_ITEMS.forest]: {
            ...messageTypes.image,
            text: i18n("story.messages.forest"),
            image: "/story/woods.jpg",
            title: i18n("story.titles.forest"),
            boxWidth: WIDER,
            imageAlt: i18n("story.mediaAlt.forest"),
        },
        [LAYOUT_ITEMS.field]: {
            ...messageTypes.image,
            text: i18n("story.messages.field"),
            image: "/story/dobrudja-bulgaria.jpg",
            title: i18n("story.titles.field"),
            boxWidth: WIDER,
            imageAlt: i18n("story.mediaAlt.field"),
        },

        [LAYOUT_ITEMS.finishFinal]: {
            component: FinishMessage,
            dismissOnSpace: true,
            stopAnimation: true,
            mainText: i18n(`story.messages.finishFinal.main`),
            gotoNextSectionText: i18n('story.messages.finishFinal.gotoNextSection'),
            continueText: i18n('story.messages.finishFinal.continue'),
            title: i18n("story.titles.finishFinal"),
            boxWidth: SHORT,
        },

    }



    return _.mapValues(messages, (message, key) => ({
        ...message,
        icon: LAYOUT_ITEMS_PROPS[key]?.icon,
    }));
}
