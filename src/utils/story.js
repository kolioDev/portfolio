import _ from 'lodash';
import { STORY_ITEMS_PROPS } from './storyItems';
import { LAYOUT_ITEMS } from '../3d/Layout/layoutItems';
import { getTechs } from './techs';
import { _ as i18n } from "svelte-i18n";

export const KEYS_FORWARD = ["ArrowRight", "d", "д", "D", "Д"];
export const KEYS_BACKWARD = ["ArrowLeft", "a","A", "а" /*cyrilic a*/, "А",/*cyrilic A*/]

const FIRST_TECH_OFFSET = 10;
const TECH_COLLISION_GAP = 1.4;

const LAST_TECH_POSITION = STORY_ITEMS_PROPS[LAYOUT_ITEMS.finish].pauseCoordinate - 15;
const techs = getTechs(()=>{});
const TECHS_LENGTH =  _.filter( techs,  (t) => "unlockIndex" in t).length;
const TECHS_STEP = (LAST_TECH_POSITION - FIRST_TECH_OFFSET) / TECHS_LENGTH;


export const techX = (tech) => (
    FIRST_TECH_OFFSET + tech.unlockIndex * TECHS_STEP
)

export const checkForUnlockedTechs = (techs, { player }) => {
    const unlocked = [];
    const filtered = _.map(techs, tech => {
        if (tech.unlocked) return tech;

        const x = techX(tech);
        const playerX = player.vehicle.position.x;
        if (x < TECH_COLLISION_GAP + playerX && x > playerX - TECH_COLLISION_GAP) {
            unlocked.push(tech.unlockIndex);
            tech.unlocked = true;
        }
        return tech;
    })

    return { filtered, unlocked };
}

