// One activity is logged only once per visit of the site 
import axios from "axios";
import qs from "qs";

export const ACTIVITIES = {
    visit: "visit",  // when the user opens the website
    arrowDown: "arrowDown", // when the user uses the arrow down
    navAbout: "navAbout",      // when the user uses the navigation button "who am I"
    navBackground: "navBackground", // when the user uses the navigation button "background"
    navTechs: "navTechs",      // when the user uses the navigation button "technologies"
    navEngineeringProjects: "navEngineeringProjects",   // when the user uses the navigation button "engineering projects"
    navSoftwareProjects: "navSoftwareProjects",   // when the user uses the navigation button "software projects"
    navHire: "navHire",      // when the user uses the navigation button "contact me"
    bannerHireMeTodayButton: "bannerHireMeTodayButton",  // when they click on the hire me today button in the banner
    aboutMore: "aboutMore",   // when the find out more is pressed on the about page
    aboutCity: "aboutCity",   // when they cycle on the different city pictures
    aboutHobies: "aboutHobies", // when the cycle on the different hobbies on the about page
    timelineButtonPrices: "timelineButtonPrices",    // when the modal with the грамоти is opened
    timelineButtonTa: "timelineButtonTa",        // when the modal with the ta jobs is opened
    timelineButtonCourses: "timelineButtonCourses",   // when the modal with the university courses is opened
    timelinePricesChangeTab: "timelinePricesChangeTab", // when the user navigates through the different tabs in the грамоти modal
    timelineTAJobDetails: "timelineTAJobDetails",    // when the user gets more details for a job
    timelineCourseDetails: "timelineCourseDetails",   // when the user gets more details for a course
    timelineCoursesSort: "timelineCoursesSort",     // when the user uses the sort mechanism
    "3dStoryOpen": "3dStoryOpen",     // when the button "yes show me the 3D story is clicked"
    strSliderUsed: "strSliderUsed",   // when they first use the slider in the 3d story
    strFinished: "strFinished",     // when they finish the "essential" part of the 3d story
    strFinishedFnl: "strFinishedFnl",  // when they completely finish the story
    storyMovedFrw: "storyMovedFrw",   // when they move forward in the 3d story
    story3dReCenter: "story3dReCenter", // when the automatic recenter is pressed
    goto2D: "goto2D",          // when story go to 2d is pressed
    storySound: "storySound",      // when the sound the 3d story is adjusted
    storyCC: "storyCC",         // when the CC button in the 3d story is pressed
    techExpand: "techExpand", // when any of the techs is expanded on the techs section
    techStackOpen: "techStackOpen",    // when any stack of technologies is opened in the project section
    engineeringSkillStackOpen: "engineeringSkillStackOpen", //when the user clicks the button "show the skills required" on the engineering project page
    projectFollowUrl: "projectFollowUrl", // when they click on any of the project urls
    linkedinOpen: "linkedinOpen",          // when the link to my linked in the contact section is clicked
    cvDownload: "cvDownload",            // when someone downloads the CV
    formTypeEmail: "formTypeEmail",         // when they start typing their email in the form
    formTypeMessage: "formTypeMessage",       // when they start typing a message in the form
    formSendMessageButton: "formSendMessageButton", // when they click the button for sending the message
    formMessageSuccess: "formMessageSuccess",    // when the message was sent successfully
    formMessageError: "formMessageError",      // message not sent successfully
    
    tajobsOpenedFromALink: "tajobsOpenedFromALink", // when the ta jobs modal is opened from a http://localhost:5000/?modal=tajobs/#story link
    coursesOpenedFromALink: "coursesOpenedFromALink", // when the ta jobs modal is opened from a http://localhost:5000/?modal=courses/#story link
    pricesOpenedFromALink: "pricesOpenedFromALink" // when the ta jobs modal is opened from a http://localhost:5000/?modal=prices/#story link
  }
  


var loggedActivities = [];

export const logActivity = (activity) => {
    // exit if the activity was already logged in
    if (loggedActivities.indexOf(activity) != -1) {
        return;
    }

    const user_agent_params = navigator.userAgent.split(" ")
    const browser = `${user_agent_params[user_agent_params.length - 1]}  ${user_agent_params[user_agent_params.length - 2]} (${navigator.appCodeName})`;

    loggedActivities.push(activity);

    axios.post(
        "/activity/log",

        qs.stringify({
            browser: browser,
            os: navigator.platform,
            activity: activity
        })
    )
        .then(() => {
        })
        .catch(({ response }) => {
            console.log("cannot log activity")
        });
}
