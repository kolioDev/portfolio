//Иконите за тенологиите
const icons = {

  "php": {
    file: "php.svg"
  },
  "laravel": {
    file: "laravel.svg"
  },
  "lumen": {
    file: "lumen.svg"
  },
  "python": {
    file: "python.svg"
  },
  "django": {
    file: "django.svg"
  },
  "tensor_flow": {
    file: "tensor_flow.svg"
  },
  "numpy": {
    file: "numPy.svg"
  },
  "go": {
    file: "go.svg"
  },
  "buffalo": {
    file: "buffalo.png"
  },
  "gorilla": {
    file: "gorilla.png"
  },
  "dbs": {
    file: "db.svg"
  },
  "mysql": {
    file: "mysql.svg"
  },
  "postgres": {
    file: "postgres.svg"
  },
  "mongodb": {
    file: "mongodb.svg"
  },
  "sqlite": {
    file: "sqlite.svg"
  },


  "css": {
    file: "css.svg"
  },
  "sass": {
    file: "sass.svg"
  },
  "semantic": {
    file: "semantic.svg"
  },
  "materialize": {
    file: "materialize.svg"
  },
  "bulma": {
    file: "bulma.png"
  },
  "foundation": {
    file: "foundation.ico"
  },
  "js": {
    file: "js.svg"
  },
  "jquery": {
    file: "jquery.svg"
  },
  "lodash": {
    file: "lodash.svg"
  },
  "vue": {
    file: "vue.svg"
  },
  "svelte": {
    file: "svelte.svg"
  },
  "angular": {
    file: "angular.svg"
  },
  "chartjs": {
    file: "chartjs.svg"
  },
  "pusher": {
    file: "pusher.svg"
  },
  "pwa": {
    file: "pwa.svg"
  },
  "html": {
    file: "html.svg"
  },
  "linux": {
    file: "linux.svg"
  },
  "ubunto": {
    file: "ubunto.svg"
  },
  "pusher": {
    file: "pusher.svg"
  },
  "arch": {
    file: "arch.svg"
  },
  "webpack": {
    file: "webpack.svg"
  },
  "nativescript": {
    file: "nativescript.svg"
  },
  "docker": {
    file: "docker.svg"
  },
  "keras": {
    file: "keras.ico"
  },
  "typescript": {
    file: "typescript.svg"
  },
  "vuex": {
    file: "vuex.png"
  },
  "apache": {
    file: "apache.svg"
  },
  "nginx": {
    file: "nginx.svg"
  },
  "postfix": {
    file: "postfix.svg"
  },
  "letencrypt": {
    file: "letencrypt.svg"
  },
  "gqlgen": {
    file: "gqlgen.svg"
  },
  "git":{
    file: "git.png"
  },
  "svn":{
    file:"svn.svg"
  },
  "fastlane":{
    file:"fastlane.svg"
  },
  "graphql":{
    file:"graphql.svg"
  },
  "jenkins":{
    file:"jenkins.svg"
  },
  "react":{
    file:"react.svg"
  },
  "redux":{
    file:"redux.svg"
  },
  "reactnative":{
    file:"react-native.svg"
  },
  "webrtc":{
    file:"webrtc.svg"
  },
  "elm":{
    file:"elm.svg"
  },

  "kivy":{
    file:"kivy.svg"
  },
  
  "engineering":{
    file:"engineering.svg"
  },

  "c":{
    file:"c.svg"
  },

  "arduino":{
    file:"arduino.svg"
  },

  "matlab":{
    file:"matlab.svg"
  },

  "simulink":{
    file:"simulink.png"
  },

  "scrum":{
    file:"scrum.svg"
  },

  "bitbucket":{
    file:"bitbucket.svg"
  },

  "confluence":{
    file:"confluence.svg"
  },

  "firebase":{
    file:"firebase.svg",
  },

  "jira":{
    file:"jira.svg"
  },

  "assembly":{
    file:"assembly.svg"
  },

  "uchase":{
    file:"uchase.png"
  },

  "bfs":{
    file:"bfs.png"
  },

  "tue":{
    file:"tue.svg"
  },

  "tue_honors":{
    file:"tue_honors.svg"
  },

  "carbyon":{
    file:"carbyon.svg"
  }
};
export default icons