// scroll to a DOM node
export const scrollToElement = (id) => {
  document.getElementById(id).scrollIntoView({ behavior: "smooth" });

  var scrollTimeout;

  const onPageScroll = () => {
    clearTimeout(scrollTimeout);
    scrollTimeout = setTimeout(function () {
      window.history.pushState(null, null, `#${id}`);
      removeEventListener('scroll', onPageScroll)
    }, 50);
  };

  addEventListener('scroll', onPageScroll)

}


export const isElementScrolledTo = (elementId, thresholdPercent) => {
  const { y, height } = document
    .getElementById(elementId)
    .getBoundingClientRect();

  if (Math.abs(y) <= (thresholdPercent / 100) * height) {
    return true
  } 
  return false
}