export const getCategories = (i18n) => ([
  //Backend
  {
    title: "Backend",
    techs: [
      //PHP
      {
        title: "php",
        subtechs: ["laravel", "lumen", "php7"]
      },
      //Python
      {
        title: "python",
        subtechs: ["django", "tensor_flow", "keras", "numpy", "kiwy"]
      },
      //Go Lang
      {
        title: "go",
        subtechs: ["buffalo", "gorilla", "gqlgen"]
      },
      //DBs
      {
        title: "dbs",
        subtechs: ["mysql", "postgres", "mongodb", "sqlite", "firebase"]
      },

      {
        title: "graphql"
      }
    ]
  },
  //Frontend
  {
    title: "Frontend",
    techs: [
      //CSS 3
      {
        title: "css",
        subtechs: ["sass", "semantic", "materialize", "bulma", "foundation"]
      },

      //JavaScript
      {
        title: "js",
        subtechs: ["typescript", "jquery", "vue", "vuex", "react", "redux", "svelte", "angular", "chartjs", "lodash", "axios", "webSockets", "pusher", "webRTC"]
      },

      //Mobile
      {
        title: "mobile",
        subtechs: ["nativescript", "reactnative", "fastlane"]
      },

      {
        title: "elm"
      },

      //PWA
      {
        title: "pwa",
      },

      // //HTML5
      // {
      //   title: "html",
      // }
    ]
  },

  //Други
  {
    title: i18n("techs.other"),
    techs: [
      //Linux 
      {
        title: "linux",
        subtechs: ["ubunto", "arch", "apache", "nginx", "postfix", "letencrypt",]
      },

      //DevOps
      {
        title: "devops",
        subtechs: ["fastlane", "jenkins", "webpack", "docker"]
      },

      {
        title: "vcs",
        subtechs: ["git", "svn"]
      },

      //Electrical engineering
      {
        title: "engineering",
        subtechs: ["c", 'assembly', "matlab", "simulink", "arduino"]
      },

      {
        title: "softwareDevelopment",
        subtechs: ["bitbucket", "jira", "confluence"]
      }

    ]
  }
]);

export const getTechs =  (i18n) =>  ({
  html: { name: "HTML5", level: 1, icon: "html", unlockIndex: 0 },
  css: { name: "CSS", level: 1, icon: "css", unlockIndex: 2 },
  js: { name: "JavaScript", level: 1, icon: "js", unlockIndex: 3 },
  jquery: { name: "JQuery", level: 1, icon: "jquery", unlockIndex: 4 },

  php: { name: "PHP", level: 0, icon: "php", unlockIndex: 5 },
  mysql: { name: "MySQL", level: 1, icon: "mysql", unlockIndex: 6 },
  laravel: { name: "Laravel", level: 0, icon: "laravel", unlockIndex: 7 },
  semantic: { name: "Semantic UI", level: 1, icon: "semantic", unlockIndex: 8 },
  linux: { name: i18n('techs.linux'), level: 1, icon: "linux", unlockIndex: 9 },
  postfix: { name: "Postfix SMTP", level: 3, icon: "postfix", unlockIndex: 10 },
  mongodb: { name: "MongoDB", level: 2, icon: "mongodb", unlockIndex: 11 },
  chartjs: { name: "ChartJS", level: 1, icon: "chartjs", unlockIndex: 12 },
  lodash: { name: "Lodash", level: 2, icon: "lodash", unlockIndex: 13 },

  python: { name: "Python", level: 2, icon: "python", unlockIndex: 14 },
  kiwy: { name: "Kivy", level: 4, icon: 'kivy', unlockIndex: 15 },
  django: { name: "Django", level: 3, icon: "django", unlockIndex: 16 },
  tensor_flow: { name: "TensorFlow", level: 2, icon: "tensor_flow", unlockIndex: 17 },
  numpy: { name: "NumPy", level: 3, icon: "numpy", unlockIndex: 18 },
  keras: { name: "KerasML", level: 4, icon: "keras", unlockIndex: 19 },

  git: { name: "GIT", level: 1, icon: "git", unlockIndex: 20 },
  apache: { name: "Apache", level: 2, icon: "apache", unlockIndex: 21 },
  letencrypt: { name: "Let's Encrypt", level: 2, icon: "letencrypt", unlockIndex: 22 },

  vue: { name: "VueJS", level: 1, icon: "vue", unlockIndex: 23 },
  materialize: { name: "Materialize CSS", level: 1, icon: "materialize", unlockIndex: 24 },
  sass: { name: "SCSS/SASS", level: 3, icon: "sass", unlockIndex: 25 },
  foundation: { name: "Foundation", level: 3, icon: "foundation", unlockIndex: 26 },

  typescript: { name: "TypeScript", level: 3, icon: "typescript", unlockIndex: 27 },
  nativescript: { name: "NativeScript", level: 2, icon: "nativescript", unlockIndex: 28 },
  angular: { name: "Angular", level: 4, icon: "angular", unlockIndex: 29 },
  webpack: { name: "WebPack", level: 3, icon: "webpack", unlockIndex: 30 },
  vuex: { name: "Vuex", level: 1, icon: "vuex", unlockIndex: 31 },

  go: { name: "Go", level: 0, icon: "go", unlockIndex: 32 },
  gorilla: { name: "Gorilla Mux ", level: 1, icon: "gorilla", unlockIndex: 33 },
  buffalo: { name: "Buffalo", level: 1, icon: "buffalo", unlockIndex: 34 },
  sqlite: { name: "SQLite", level: 1, icon: "sqlite", unlockIndex: 35 },
  postgres: { name: "PostgreSQL", level: 2, icon: "postgres", unlockIndex: 36 },
  bulma: { name: "Bulma", level: 2, icon: "bulma", unlockIndex: 37 },



  nginx: { name: "Nginx", level: 2, icon: "nginx", unlockIndex: 38 },
  docker: { name: "Docker", level: 4, icon: "docker", unlockIndex: 39 },

  webRTC: { name: "webRTC", level: 4, icon: "webrtc", unlockIndex: 40 },
  pusher: { name: "Pusher", level: 2, icon: "pusher", unlockIndex: 41 },
  pwa: { name: "PWA", level: 2, icon: "pwa", unlockIndex: 42 },

  svelte: { name: "Svelte", level: 2, icon: "svelte", unlockIndex: 43 },
  lumen: { name: "Lumen", level: 4, icon: "lumen", unlockIndex: 44 },
  react: { name: "React", level: 3, icon: "react", unlockIndex: 45 },
  redux: { name: "Redux", level: 2, icon: "redux", unlockIndex: 46 },
  reactnative: { name: "React Native", level: 0, icon: "reactnative", unlockIndex: 47 },
  svn: { name: "SVN", level: 2, icon: "svn", unlockIndex: 48 },
  fastlane: { name: "Fastlane", level: 3, icon: "fastlane", unlockIndex: 49 },
  jenkins: { name: "Jenkins", level: 3, icon: "jenkins", unlockIndex: 50 },

  firebase: { name: "Firebase", level: 2, icon: "firebase", unlockIndex: 51 },
  jira: { name: "Jira", level: 2, icon: "jira", unlockIndex: 52 },
  bitbucket: { name: "Bitbucket", level: 1, icon: "bitbucket", unlockIndex: 53 },
  confluence: { name: "Confluence", level: 2, icon: "confluence", unlockIndex: 54 },

  elm: { name: "Elm", level: 2, icon: "elm", unlockIndex: 55 },
  graphql: { name: "GraphQL", level: 3, icon: "graphql", unlockIndex: 56 },
  gqlgen: { name: "gqlgen", level: 3, icon: "gqlgen", unlockIndex: 57 },

  c: { name: "C", level: 3, icon: "c", unlockIndex: 58 },
  assembly: { name: "MIPS Assembly", level: 2, icon: "assembly", unlockIndex: 59 },
  arduino: { name: "Arduino", level: 4, icon: "arduino", unlockIndex: 60 },
  matlab: { name: "Matlab", level: 2, icon: "matlab", unlockIndex: 61 },
  simulink: { name: "Simulink", level: 3, icon: "simulink", unlockIndex: 62 },


  //NOT PART OF STORY
  php7: { name: "PHP 7", level: 1, icon: false },
  dbs: { name: i18n("techs.dbs"), level: 1, icon: "dbs" },
  axios: { name: "AxiosHTTP", level: 1, icon: false },
  webSockets: { name: "WebSockets", level: 1, icon: false },
  ubunto: { name: "Ubunto", level: 1, icon: "ubunto" },
  arch: { name: "ArchLinux", level: 1, icon: "arch" },
  mobile: { name: "Mobile", level: 1, icon: false },
  devops: { name: "DevOps", level: 3, icon: false },
  vcs: { name: "Version control", level: 1, icon: "git" },
  engineering: { name: i18n("techs.engineering"), level: 2, icon: "engineering" },
  softwareDevelopment: { name: i18n("techs.softwareDevelopment"), level: 2, icon: "scrum" },


});

