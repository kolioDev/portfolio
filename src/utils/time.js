export const yearsSince = (dateString) => {
    const date = new Date(dateString);

    return Math.floor(
        (new Date() - date) / (1000 * 60 * 60 * 24 * 365.25)
    );
}