const getContent = (key, i18n) => ({
    title: i18n(`unicourses.${key}.title`),
    objective: i18n(`unicourses.${key}.objective`),
    content: i18n(`unicourses.${key}.content`),
})

export const TYPE_MAJOR = 'MAJOR';
export const TYPE_ELECTIVE = 'ELECTIVE';
export const TYPE_BASIC = 'BASIC';
export const TYPE_USE = 'USE';
export const TYPE_OTHER = 'OTHER';
export const TYPE_BEP = 'BEP';

export const LINK_TUE_OSIRIS = 'LINK_TUE_OSIRIS';
export const LINK_X_SYNAPSE = 'LINK_X_SYNAPSE'; 

export const getUniCoursesList = (i18n) => ([
    {
        code: '2IWA0',  //Automotive software engineering
        year: 3,
        type: TYPE_MAJOR,
        grade: 8,
        final_exam_grade: 8.4,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('2IWA0', i18n)

    },
    {
        code: '5AID0', //DBL Autonomous vehicles
        year: 3,
        type: TYPE_MAJOR,
        grade: 8,
        final_exam_grade: -1,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5AID0', i18n)
    },
    {
        code: '5XSK0', //Data fusion & semantic interpretation
        year: 3,
        type: TYPE_OTHER,
        grade: 7,
        final_exam_grade: 5.7,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XSK0', i18n)
    },
    {
        code: '5XWB0', // Electric drive systems
        year: 3,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 8.9,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XWB0', i18n)
    },
    {
        code: '5XWC0', // Energy management
        year: 3,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 8.7,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XWC0', i18n)
    },
    {
        code: '2WBB0', //Calculus variant 2
        year: 1,
        type: TYPE_BASIC,
        grade: 9,
        final_exam_grade: 9.3,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('2WBB0', i18n)
    },
    {
        code: '3NBB0', //Applied natural sciences formal
        year: 1,
        type: TYPE_BASIC,
        grade: 9,
        final_exam_grade: 9.6,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('3NBB0', i18n)
    },
    {
        code: '0SAB0', // USE basic: ethics and history of technology
        year: 1,
        type: TYPE_BASIC,
        grade: 7,
        final_exam_grade: -1,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('0SAB0', i18n)
    },
    {
        code: '2IAB0', // Data analytics for engineers
        year: 1,
        type: TYPE_BASIC,
        grade: 7,
        final_exam_grade: 6.7,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('2IAB0', i18n)
    },
    {
        code: '5ATA0', // Spectrum of automotive
        year: 1,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.1,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5ATA0', i18n)
    },
    {
        code: '5EIA0', // Computation I: hardware/software interface
        year: 1,
        type: TYPE_MAJOR,
        grade: 10,
        final_exam_grade: 9.8,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5EIA0', i18n)

    },
    {
        code: '5ESE0', // Signal processing basics(Signals I) 
        year: 1,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 10.0,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5ESE0', i18n)
    },
    {
        code: '5ASC0', // Dynamics for automotive applications
        year: 1,
        type: TYPE_MAJOR,
        grade: 10,
        final_exam_grade: 9.8,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5ASC0', i18n)
    },
    {
        code: '2DE20', // Mathematics 1 
        year: 1,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.6,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('2DE20', i18n)
    },
    {
        code: '5ESB0', // Systems
        year: 1,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.3,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5ESB0', i18n)
    },
    {
        code: '4WBB0', // Engineering design
        year: 1,
        type: TYPE_BASIC,
        grade: 8,
        final_exam_grade: -1,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('4WBB0', i18n)
    },
    {
        code: '4AUB20', // Road Vehicle Dynamics
        year: 2,
        type: TYPE_MAJOR,
        grade: 10,
        final_exam_grade: 9.5,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('4AUB20', i18n)
    },
    {
        code: '5EPA0', // Electromagnetics I
        year: 2,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 8.4,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5EPA0', i18n)
    },
    {
        code: '5XCA0', // Fundamentals of electronics
        year: 2,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.5,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XCA0', i18n)
    },
    {
        code: '5EWA0', // Electromechanics 
        year: 2,
        type: TYPE_MAJOR,
        grade: 8,
        final_exam_grade: 8.1,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5EWA0', i18n)
    },
    {
        code: '5APA0', // Power Electronics 
        year: 2,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.2,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5APA0', i18n)
    },
    {
        code: '5AIB0', // Sensing computing and actuating
        year: 2,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 9.3,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5AIB0', i18n)
    },
    {
        code: '4AUB10', // Electric and Hybrid vehicles power train design 
        year: 2,
        type: TYPE_MAJOR,
        grade: 8,
        final_exam_grade: 8.6,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('4AUB10', i18n)
    },
    {
        code: '5AIC0', // Vehicle Networking
        year: 3,
        type: TYPE_MAJOR,
        grade: 7,
        final_exam_grade: 7.0,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5AIC0', i18n)
    },
    {
        code: '5ESD0', //Control System
        year: 3,
        type: TYPE_MAJOR,
        grade: 9,
        final_exam_grade: 8.3,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5ESD0', i18n)
    },
    {
        code: '1CK00', //Transport and distribution
        year: 1,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 9.0,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('1CK00', i18n)
    },
    {
        code: '4PB00', //Heat and flow 
        year: 2,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 9.0,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('4PB00', i18n)
    },
    {
        code: 'INF473X', //The Hacker's eXperience
        year: 2,
        type: TYPE_ELECTIVE,
        grade: 'A',
        final_exam_grade: -1,
        level: -1,
        linkType: LINK_X_SYNAPSE,
        ...getContent('INF473X', i18n)
    },
    {
        code: '5XSJ0', //Automotive Sensing
        year: 3,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 8.9,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XSJ0', i18n)
    },
    {
        code: 'SFC500', //Dutch for beginners I
        year: 2,
        type: TYPE_OTHER,
        grade: 8,
        final_exam_grade: 7.5,
        level: -1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('SFC500', i18n)
    },
    {
        code: 'SFC510', //Dutch for beginners II
        year: 2,
        type: TYPE_OTHER,
        grade: 6,
        final_exam_grade: 5.9,
        level: -1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('SFC510', i18n)
    },
    {
        code: '5XIA0', //DBL energy challenge 
        year: 1,
        type: TYPE_ELECTIVE,
        grade: 9,
        final_exam_grade: 9.4,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XIA0', i18n)
    },
    {
        code: '5XWF0', //Wireless charging
        year: 1,
        type: TYPE_ELECTIVE,
        grade: 8,
        final_exam_grade: -1,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XWF0', i18n)
    },
    {
        code: '5XSC0',//DBL Automotive design project electronic differential
        year: 3,
        type: TYPE_ELECTIVE,
        grade: 8,
        final_exam_grade: -1,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XSC0', i18n)
    },
    {
        code: '0SEUB0',//Patents 1
        year: 2,
        type: TYPE_USE,
        grade: 8,
        final_exam_grade: 8.3,
        level: 1,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('0SEUB0', i18n)
    },
    {
        code: '0SSUC0',//Patents 2
        year: 2,
        type: TYPE_USE,
        grade: 8,
        final_exam_grade: 8.6,
        level: 2,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('0SSUC0', i18n)
    },
    {
        code: '0SAUC0',//Patents 3
        year: 2,
        type: TYPE_USE,
        grade: 7,
        final_exam_grade: -1,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('0SAUC0', i18n)
    },
    {
        code:'5XEC0', //BEP
        year: 3, 
        type: TYPE_BEP,
        grade: 9,
        final_exam_grade: -1,
        level: 3,
        linkType: LINK_TUE_OSIRIS,
        ...getContent('5XEC0', i18n)
    }

])
