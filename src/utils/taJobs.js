const getContent = (key, i18n) => ({
    responsibilities: i18n(`taJobs.${key}`),
    title: i18n(`unicourses.${key}.title`)
})


export const getTaJobsList = (i18n) => ([
    // Electromagnetics
    {
        code: "5EPA0",
        period :{
            from: "01/09/2023",
            to: "31/10/2023",
        },
        ...getContent("5EPA0", i18n),
    },

    //  Mathematics 1
    {
        code: "2DE20",
        period :{
            from: "01/02/2023",
            to: "30/04/2023",
        },
        ...getContent("2DE20", i18n),
    },

    // Electromechanics
    {
        code: "5EWA0",
        period :{
            from: "01/12/2023",
            to: "29/02/2024",
        },
        ...getContent("5EWA0", i18n),
    },

    // Spectrum of Automotive
    {
        code: "5ATB0",
        period :{
            from: "16/11/2023",
            to: "15/04/2024",
        },
        ...getContent("5ATB0", i18n),
    },


    //Computation 
    {
        code: '5EIA0',
        period :{
            from: "01/09/2022",
            to: "31/10/2022",
        },
        ...getContent("5EIA0", i18n),
    },

    // Dynamics
    {
        code: "5ASC0",
        period :{
            from: "01/02/2023",
            to: "30/04/2023",
        },
        ...getContent("5ASC0", i18n),
    },


    // Signals and Systems
    {
        code: "5ESF0",
        period :{
            from: "1/11/2023",
            to: "31/01/2024",
        },
        ...getContent("5ESF0", i18n),
    },
    // Systems
    {
        code: "5ESB0",
        period :{
            from: "15/04/2023",
            to: "30/07/2023",
        },
        ...getContent("5ESB0", i18n),
    },





])