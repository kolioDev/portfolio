export const getEngineeringProjects = (i18n)=>([
  // Electric Drive Systems
  {
    ...getContent("electricDrives", i18n),
    img: "projects/electricDrives.png",
  },
   // InCharge
   {
    ...getContent("inCharge", i18n),
    img: "projects/inverter.png",
  },
   // Energy Management
   {
    ...getContent("energyManagement", i18n),
    img: "projects/energyManagement.png",
  },
]);


const getContent=(key, i18n)=>(
  {
    name: i18n(`engineeringProjects.${key}.title`),
    description: i18n(`engineeringProjects.${key}.description`),
    tools: i18n(`engineeringProjects.${key}.tools`),
    skills: i18n(`engineeringProjects.${key}.skills`),
  }
)