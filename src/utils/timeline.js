const getContent = (key, i18n) => ({
    title: i18n(`storyShort.${key}.title`),
    text: i18n(`storyShort.${key}.text`),
    titleLinkText: i18n(`storyShort.${key}.titleLink`),
})

const TIMELINE_TYPE_EDUCATION  = 'TIMELINE_TYPE_EDUCATION';
const TIMELINE_TYPE_EMPLOYMENT = 'TIMELINE_TYPE_EMPLOYMENT';

export const BUTTON_TYPE_COURSES = "BUTTON_TYPE_COURSES";
export const BUTTON_TYPE_PRICES  = "BUTTON_TYPE_PRICES";
export const BUTTON_TYPE_TA      = "BUTTON_TYPE_TA";

export const getType = (i18n)=>({
    TIMELINE_TYPE_EDUCATION: i18n('storyShort.timelineEducation'),
    TIMELINE_TYPE_EMPLOYMENT: i18n('storyShort.timelineEmployment')
})

export const getTimeline = (i18n) => ([
    {
        period:'2024-now',
        type:TIMELINE_TYPE_EMPLOYMENT,
        titleLinkUrl:"https://carbyon.com",
        ...getContent('carbyon', i18n),
         logo:"carbyon"
    },
    {
        period: '2022-2024',
        type: TIMELINE_TYPE_EDUCATION,
        titleLinkUrl:"https://studiegids.tue.nl/opleidingen/tue-honors-academy/",
        ...getContent('honors', i18n),
        logo:"tue_honors",
    },
    {
        period: '2022-2024',
        type:TIMELINE_TYPE_EMPLOYMENT,
        titleLinkUrl: 'https://tue.nl',
        ...getContent('tutoring', i18n),
        logo:"tue",
        buttonText:i18n('storyShort.tutoring.button'),
        button: BUTTON_TYPE_TA
        // coursesButton: true
    },
    {
        period: '2021-2024',
        type: TIMELINE_TYPE_EDUCATION,
        titleLinkUrl: 'https://tue.nl',
        ...getContent('TUe', i18n),
        text: i18n(`storyShort.TUe.text`, {
            values: {
                credits: webscrapedData.passed * 5,
                total_credits: 180,
            }
        }),
        logo:"tue",
        buttonText:i18n('storyShort.TUe.button'),
        button: BUTTON_TYPE_COURSES
    },
    {
        period: '2020-2021',
        type:TIMELINE_TYPE_EMPLOYMENT,
        titleLinkUrl: 'https://ucha.se',
        logo:"uchase",
        ...getContent('uchase', i18n)
    },
    {
        period: '2015-2020',
        type:TIMELINE_TYPE_EDUCATION,
        title: i18n(`storyShort.competitions.title`),
        buttonText:i18n('storyShort.competitions.button'),
        button: BUTTON_TYPE_PRICES
    },
    {
        period: '2019-2021',
        type:TIMELINE_TYPE_EMPLOYMENT,
        titleLinkUrl: 'https://bfunion.bg/',
        logo:"bfs",
        ...getContent('referee', i18n)
    },
    {
        period: '2015-2020',
        type:TIMELINE_TYPE_EDUCATION,
        titleLinkUrl: 'https://eg-dobrich.com',
        ...getContent('EG', i18n)
    },
    {
        period: '2012-2015',
        type:TIMELINE_TYPE_EDUCATION,
        titleLinkUrl: 'https://pmg-dobrich.com',
        ...getContent('PMG', i18n)
    },







])