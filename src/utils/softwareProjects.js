export const getSoftwareProjects = (i18n)=>([
  {
    // BANANAMIN
    name: "BananaMin",
    url: "https://bananamin.koliodev.com",
    description: i18n('softwareProjects.bananamin'),
    img: "projects/bananamin.png",
    techs: {
      backend: ["go", "buffalo", "gorilla", "postgres", "mysql"],
      frontend: [
        "css",
        "sass",
        "materialize",
        "vue",
        "vuex",
        "jquery",
        "chartjs",
        "lodash",
        "webSockets",
      ],
      other: ["ubunto", "nginx", "webpack"],
    },
  },
  //Виртуалната оптика
  {
    name: "OnТика",
    url: "https://opticans.koliodev.com",
    description: i18n('softwareProjects.opticans'),
    img: "projects/opticans.png",
    techs: {
      backend: ["php", "laravel", "mysql"],
      frontend: ["semantic", "vue", "css", "jquery", "chartjs"],
      other: ["ubunto", "apache", "webpack"],
    },
  },
  //TheSmartHelper
  {
    name: "TheSmartHelper",
    url: false,
    description: i18n('softwareProjects.thesmarthelper'),
    img: "projects/thesmarthelper.png",
    techs: {
      backend: [
        "php",
        "laravel",
        "mysql",
        "mongodb",
        "python",
        "tensor_flow",
        "keras",
        "numpy",
      ],
      frontend: ["css", "semantic", "vue", "jquery", "lodash"],
      other: [
        "nativescript",
        "angular",
        "typescript",
        "ubunto",
        "apache",
        "webpack",
      ],
    },
  },
]);