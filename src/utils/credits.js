export const credits = [
    {
        "Name": "Road by Poly by Google",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/a38EEOJFUlp",
        "LinkName":"Poly Pizza",
    },
    {
        "Name": "Stop sign by Poly by Google",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/60GyU9CdZ9r",
        "LinkName":"Poly Pizza",
    },
    {
        "Name": "Cargo Train Wagon by Quaternius",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/aD8HbWy8fD",
        "LinkName":"Poly Pizza",
    },
    {
        "Name": "Cargo Train Front by Quaternius",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/CdAKX30G6x",
        "LinkName":"Poly Pizza",
    },
    {
        "Name": "Battery by zeoxo",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/9dtxNKBIhAW",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "Trees by Poly by Google",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/dTy_L-TMS2z",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "Field by Poly by Google",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/crMd2fnb17X",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "Mountain by jeremy",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/0Fl55ZzsVzT",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "Table tennis table by jeremy",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/c_BDr31X5_-",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "Low Poly Guys by Don Carson",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/9f7_Qqt4Mj3",
        "LinkName":"Poly Pizza"
    },
    {
        "Name": "server rack by Jeremy Eyring",
        "CC-BY": true,
        "Link": "https://poly.pizza/m/6ijQclm8jxw",
        "LinkName":"Poly Pizza"
    },
    {
        "Name":"Sound effects obtained from",
        "CC-BY":false,
        "Link":"https://www.zapsplat.com",
        "LinkName":"zapsplat (zapsplat.com)"
    }
]

// Name [CC-BY] (https://creativecommons.org/licenses/by/3.0/) via Poly Pizza (https://poly.pizza/m/6ijQclm8jxw)