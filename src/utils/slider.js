import _ from "lodash";

import { STORY_ITEMS_PROPS } from "./storyItems";

export const getStopsProps = ()=>{
   return  _.values(
        _.mapValues(STORY_ITEMS_PROPS, (value, key) => {
            return {
                ...value,
                key,
            };
        })
    );
}

const getMinStartEnd = (stopsProps) => {
    const minStart = -_.first(stopsProps).pauseCoordinate;
    const maxEnd =
        _.last(stopsProps).pauseCoordinate +
        _.first(stopsProps).pauseCoordinate;

    return [minStart, maxEnd]
}


export const calculateSliderValue = (playerPosition) => {
    const stopsProps = getStopsProps();

    //interpolates the value of the slider between 2 nearests stops
    const [minStart, maxEnd] = getMinStartEnd(stopsProps);

    let min = null;
    let max = null;
    let currentStopIndex = 0;

    if (playerPosition < _.first(stopsProps).pauseCoordinate) {
        min = minStart;
        max = _.first(stopsProps).pauseCoordinate;
        currentStopIndex = 0;
    }
    if (playerPosition > _.last(stopsProps).pauseCoordinate) {
        min = _.last(stopsProps).pauseCoordinate;
        max = maxEnd;
        currentStopIndex = stopsProps.length;
    } else {
        _.each(stopsProps, (stop, index) => {
            if (
                playerPosition >= stop.pauseCoordinate &&
                playerPosition < stopsProps[index + 1].pauseCoordinate
            ) {
                min = stop.pauseCoordinate;
                max = stopsProps[index + 1].pauseCoordinate;
                currentStopIndex = index + 1;
            }
        });
    }

    const localInterp = ((playerPosition - min) / (max - min)) * 100;

    const interpolationShifted =
        localInterp / (stopsProps.length + 1) - //gloabl interpolated
        50 / (stopsProps.length + 1) + //initial shift
        (currentStopIndex * 100) / stopsProps.length; //previous stops shift

    return interpolationShifted;
};

export const calculatePlayerPosition = (sliderValue) => {
    const stopsProps = getStopsProps();

    const [minStart, maxEnd] = getMinStartEnd(stopsProps);
    const stopSize = 100 / stopsProps.length;

    const stopRelPos = sliderValue / stopSize - 0.5;
    const stopIndex = Math.floor(stopRelPos);

    let min = null;
    let max = null;
    
    if (stopIndex === -1) {
        min = minStart;
        max = _.first(stopsProps).pauseCoordinate;
    } else if (stopIndex >= stopsProps.length - 1) {
        min = _.last(stopsProps).pauseCoordinate;
        max = maxEnd;
    } else {
        min = stopsProps[stopIndex].pauseCoordinate;
        max = stopsProps[stopIndex + 1].pauseCoordinate;
    }

    const pos = stopRelPos - stopIndex;

    const localInterp = pos * (max - min);
    const globalInterp = min + localInterp;

    return globalInterp;
};