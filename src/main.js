import "./i18n/dict.js"
import "./i18n/config.js"
import App from './App.svelte';

const app = new App({
  target: document.body,
  props: {}
});

export default app;