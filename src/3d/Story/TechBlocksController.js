import { TechBlock } from "./TechBock";
import _ from 'lodash'
import icons from "../../utils/icons.js";
import { checkForUnlockedTechs, techX } from "../../utils/story";

//TODO:optimize so that only visible techs are rendered

export class TechBlocksController {
    #scene
    #techs

    #blocks = {};

    constructor({ scene, techs }) {
        this.#scene = scene;
        this.#techs = techs;
    }

    async Init() {
        for (const tech of this.#techs) {
            const techBlock = new TechBlock({
                icon: "/icons/" + icons[tech.icon].file,
                xPosition: techX(tech)
            })

            await techBlock.Load();
            this.#scene.add(techBlock.object);

            this.#blocks[tech.unlockIndex] = techBlock;
        }
    }

    Animate(deltaT, t) {
        for (const k in this.#blocks) {
            const block = this.#blocks[k]
            const { die } = block.Animate(deltaT, t);
            if (die) {
                this.#scene.remove(block.object);
                delete (block.destructor());
            }
        }
    }

    UpdateTechs(player) {
        const { unlocked, filtered } = checkForUnlockedTechs(this.#techs, { player });
        if (unlocked.length === 0) {
            return { techs: this.#techs, updated: false };
        }

        for (const k of unlocked) {
            this.#blocks[k].TriggerDeathAnimation();
        }



        this.#techs = filtered;
        return { techs: this.#techs, updated: true };
    }
}