import * as THREE from 'three';

import { GLTF } from '../Gltf.js';
import { SoundManager } from '../Managers/SoundManager.js';

const LOCAL_X = -0.04;
const LOCAL_Y = 1.126;

const UNLOCK_SOUND = 'unlock_tech'; 

const ANIMATION_PROPS = {
    angle: 30 * Math.PI / 180,//30deg
    frequency: .002,
    deathFrequency: 0.01,
    deathAnimationBounce: 0.007,
    phase: Math.PI / 8,
    deathAnimationDuration: 270 //ms 
}

class TechBlockGLTF extends GLTF {
    _gltf;
    _filename = 'tech_block.glb';
    _z = -0.1;

    async Load() {
        await super.Load();

        this._gltf.scene.traverse(child => {
            if (child.isMesh) {
                if (child.name === "OuterCube") {
                    child.material = new THREE.MeshBasicMaterial({
                        color: 0xfcc923//fb2929,
                    });
                } else {
                    child.material = new THREE.MeshBasicMaterial({
                        color: 'white',
                    });
                }
                child.castShadow = true;
            }
        });
    }
}


export class TechBlock {
    #group;
    #iconName;
    #xPosition;

    #gltfMesh;
    #iconMesh;

    #deathAnimationTriggered = false;
    #deathAnimationTime = 0;

    
    static #gltf;
    static #basicIconMesh;
    static #soundManager;

    constructor({ icon, xPosition }) {
        this.#group = new THREE.Group();
        this.#xPosition = xPosition;
        this.#iconName = icon;

        if(!TechBlock.#soundManager){
            TechBlock.#soundManager = new SoundManager;
            TechBlock.#soundManager.RegisterSound(UNLOCK_SOUND, 'sounds/gem.mp3')
        }
    }

    destructor() {
        this.#destroy();
    }

    async Load() {
        await this.loadGltfBlock();
        this.loadIconBlock();


        this.#group.add(this.#gltfMesh);
        this.#group.add(this.#iconMesh);

        this.#group.position.x = this.#xPosition;
        this.#group.position.y = LOCAL_Y;
    }

    async loadGltfBlock() {
        if (!TechBlock.#gltf) {
            TechBlock.#gltf = new TechBlockGLTF();
            await TechBlock.#gltf.Load();
        }

        this.#gltfMesh = TechBlock.#gltf.scene.clone();
        this.#gltfMesh.position.x = LOCAL_X
        this.#gltfMesh.position.y = -LOCAL_Y;
    }

    loadIconBlock() {
        if (!TechBlock.#basicIconMesh) {
            const iconGeometry = new THREE.BoxBufferGeometry(.24, .24, .001);
            // prepare geometry to use 2 materials
            iconGeometry.clearGroups();
            iconGeometry.addGroup(0, Infinity, 0);
            iconGeometry.addGroup(0, Infinity, 1);

            // create 2 materials
            const whiteMaterial = new THREE.MeshBasicMaterial({
                color: 'white',
            });

            TechBlock.#basicIconMesh = new THREE.Mesh(iconGeometry, [whiteMaterial]);

        }


        const iconMaterial = this.#createTextureMaterial();


        this.#iconMesh = new THREE.Mesh(
            TechBlock.#basicIconMesh.geometry.clone(),
            [TechBlock.#basicIconMesh.material[0], iconMaterial]

        );

        this.#iconMesh.position.z = .24;
        this.#iconMesh.position.y = 0;
        this.#iconMesh.position.x = LOCAL_X;
    }

    #createTextureMaterial() {
        const iconTexture = new THREE.TextureLoader().load(this.#iconName);//load("/icons/mysql.svg");
        iconTexture.wrapS = THREE.ClampToEdgeWrapping;
        iconTexture.wrapT = THREE.ClampToEdgeWrapping;
        iconTexture.flipY = true;
        iconTexture.repeat.set(1, 1);

        const iconMaterial = new THREE.MeshBasicMaterial({
            map: iconTexture,
            transparent: true,
        });

        return iconMaterial
    }


    Animate(deltaT, t) {
        if (this.#deathAnimationTriggered) {
            const psi = t * ANIMATION_PROPS.deathFrequency;
            this.#group.rotation.z = psi
            this.#group.position.x += deltaT* ANIMATION_PROPS.deathAnimationBounce;
            this.#group.position.y += deltaT* ANIMATION_PROPS.deathAnimationBounce;

            this.#deathAnimationTime += deltaT
            if(this.#deathAnimationTime >= ANIMATION_PROPS.deathAnimationDuration){
                return {die:true};
            }
        }

        let phi = ANIMATION_PROPS.angle * Math.cos(t * ANIMATION_PROPS.frequency + this.#xPosition * ANIMATION_PROPS.phase);
        this.#group.rotation.y = phi


        return {die:false};
    }


    TriggerDeathAnimation() {
        this.#deathAnimationTriggered = true;
        TechBlock.#soundManager.PlaySound(UNLOCK_SOUND);
    }


    #destroy() {
        this.#group.traverse(child => {
            if (child.dispose) {
                child.dispose()
            }
        });
    }

    get object() {
        return this.#group;
    }
}

