export const DEBUG = false;

import * as THREE from 'three';
import _ from 'lodash';

//  Uncomment when debugging
// import * as dat from 'dat.gui';
// import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

//TODO:препоръки от Ванката 
// 1. Скипва стоповете с нинджа движение 
// [2.] Планинкското колело рамката и гумите и изглеждат тънки
// [3.] Skip button на истрункциите 

//TODO: feedback от Павката 
// [1.] Елементите не се кликат в горната част на екрана
// 2. Назад не бачка понякога
// [3.] Иконите в "раницата" не се показват целите 

//TODO:
// [1.] да добавя бутон в навигацията

//TODO: от наско
// [1.] Модалите да се затварят с esc 
// 2. raycast-a да се disable-ва с delay след затваряне на съобщение
// [3.] На мусала да пренапиша "almost solo"


import { Layout } from './Layout/Layout';
import { Camera } from './Camera/Camera';
import { Player } from './Player/Player';
import { Light } from './Light/Light';

import { ClickHandler } from './Managers/ClickHandler';
import { StoryPauseManager } from './Managers/StoryPauseManager';
import { TechBlocksController } from './Story/TechBlocksController';
import { Debugger, EmptyDebugger } from './Managers/Debuger';
import { SoundManager } from './Managers/SoundManager';
import { getTechs } from "../utils/techs";


const debug = {
    orbitControl: 0
};
if (DEBUG) {
    const gui = new dat.GUI()
    gui.add(debug, 'orbitControl', 0, 1);
}

const MyDebugger = DEBUG ? new Debugger() : new EmptyDebugger();

export class Scene {

    #scene;
    #camera;
    #light;
    #player;
    #layout;

    #renderer;
    #clickHandler;
    #techBlocksController;
    #storyPauseManager;
    #soundManager;

    #sceneWidth;
    #sceneHeight;

    #rootElement;

    #animationTime = 0;
    #animationStopped = false;

    #eventHandlers = {
        storyPause: () => { },
        techsUnlocked: () => { },
        playerPositionChanged: () => { },
    }

    #techs;

    //Only used when DEBUG = true
    orbitControl

    constructor({ sceneWidth, sceneHeight, rootElement, eventHandlers, i18n }) {
        this.#sceneWidth = sceneWidth;
        this.#sceneHeight = sceneHeight
        this.#rootElement = rootElement;
        this.#eventHandlers = eventHandlers;

        const allTechs = getTechs(i18n);
        this.#techs = _.filter(
            _.map(allTechs, (t) => {
                t.unlocked = false;
                return t;
            }),
            (t) => "unlockIndex" in t
        );

        this.#eventHandlers.techsUnlocked(this.#techs);

        this.#soundManager = new SoundManager();
    }

    async Initialize() {
        this.#scene = new THREE.Scene();

        // ADDING BACKGROUND AND FOG
        const fogColor = new THREE.Color(0xc8c9bb)//THREE.Color(0xb7ba98);
        this.#scene.background = fogColor;
        this.#scene.fog = new THREE.FogExp2(fogColor, 0.058);//new THREE.Fog(fogColor, 0.1, 35);

        this.#renderer = new THREE.WebGLRenderer({
            //fixes broken straigt lines
            antialias: true,
            powerPreference: "high-performance",
            // logarithmicDepthBuffer: true,
        });
        this.#renderer.setSize(this.#sceneWidth, this.#sceneHeight);
        this.#renderer.shadowMap.enabled = true;

        this.#rootElement.appendChild(this.#renderer.domElement);

        //Player init
        this.#player = new Player();
        await this.#player.Load();
        this.#scene.add(this.#player.vehicle.scene);

        //Camera init
        this.#camera = new Camera({
            target: this.#player.vehicle,
            sceneWidth: this.#sceneWidth,
            sceneHeight: this.#sceneHeight,
            player: this.#player
        });

        //Click handler init
        this.#clickHandler = new ClickHandler({
            camera: this.#camera.camera,
            domElement: this.#renderer.domElement
        })

        this.#player.AttachClickHandler();


        //Story pause manager init
        this.#storyPauseManager = new StoryPauseManager({
            player: this.#player,
            scene: this.#scene
        });
        await this.#storyPauseManager.Load();

        //Lights init
        this.#light = new Light({
            target: this.#player.vehicle.scene
        });
        this.#scene.add(this.#light.lights);

        //Layout init
        this.#layout = new Layout({
            scene: this.#scene,
            fieldOfView: this.#camera.fieldOfView,
            player: this.#player
        });
        await this.#layout.Init();

        if (DEBUG) {
            this.orbitControl = new OrbitControls(this.#camera.camera, this.#renderer.domElement);
        }


        //Intentionally do not await
        this.#addTechs();

    }

    async #addTechs() {
        this.#techBlocksController = new TechBlocksController({ scene: this.#scene, techs: this.#techs });
        await this.#techBlocksController.Init();
    }


    Animate() {
        requestAnimationFrame((t) => {

            let deltaT = t - this.#animationTime;
            this.#animationTime = t;

            const oldPlayerPosition = this.#player.vehicle.position.x;



            if (!this.#animationStopped) {
                MyDebugger.LogSampleStart("General");


                MyDebugger.LogSampleStart("Player");
                this.#player.Animate(deltaT);
                MyDebugger.LogSampleEnd("Player");

                MyDebugger.LogSampleStart("PauseManager");
                const { releaseThrottle, showMessage } = this.#storyPauseManager.Animate(deltaT);
                releaseThrottle ? this.#player.vehicle.ThrottleRelease() : null;
                showMessage ? this.#eventHandlers.storyPause(showMessage) : null;
                MyDebugger.LogSampleEnd("PauseManager");


                if (!DEBUG || debug.orbitControl == 0) {
                    MyDebugger.LogSampleStart("Camera");
                    this.#camera.Animate(deltaT);
                    MyDebugger.LogSampleEnd("Camera");

                    MyDebugger.LogSampleStart("Layout");
                    this.#layout.Animate({
                        fieldOfView: this.#camera.fieldOfView,
                        deltaT
                    });
                    MyDebugger.LogSampleEnd("Layout");
                }

                MyDebugger.LogSampleStart("Light");
                this.#light.Animate(deltaT);
                MyDebugger.LogSampleEnd("Light");

                if (DEBUG && debug.orbitControl > 0) {
                    this.orbitControl.update();
                }

                if (this.#techBlocksController) {
                    MyDebugger.LogSampleStart("TechBlocks");
                    this.#techBlocksController.Animate(deltaT, t);
                    const { techs, updated } = this.#techBlocksController.UpdateTechs(this.#player);
                    if (updated) {
                        this.#techs = techs;
                        this.#eventHandlers.techsUnlocked(techs);
                    }
                    MyDebugger.LogSampleEnd("TechBlocks");
                }


                MyDebugger.LogSampleStart("Renderer");
                this.#renderer.render(this.#scene, this.#camera.camera);
                MyDebugger.LogSampleEnd("Renderer");


                MyDebugger.LogSampleStart("Player x position callback");
                if (oldPlayerPosition !== this.#player.vehicle.position.x) {
                    this.#eventHandlers.playerPositionChanged(this.#player.vehicle.position.x);
                }
                MyDebugger.LogSampleEnd("Player x position callback");


                MyDebugger.LogSampleEnd("General");
            }


            this.Animate();
        });
    }

    Resize({ sceneWidth, sceneHeight }) {
        this.#sceneWidth = sceneWidth;
        this.#sceneHeight = sceneHeight;

        this.#camera.Resize({ sceneWidth, sceneHeight });

        this.#renderer.setSize(sceneWidth, sceneHeight);
    }

    HandleClick(e) {
        return this.#clickHandler.onClick(e);
    }

    ThrottleBackward() {
        if (this.#storyPauseManager.shouldStopBackward) return;
        this.#player.vehicle.ThrottleBackward();
    }
    ThrottleForward() {
        if (this.#storyPauseManager.shouldStopForward) return;
        this.#player.vehicle.ThrottleForward();
    }
    ThrottleRelease() {
        this.#player.vehicle.ThrottleRelease();
    }

    ChangeVolume(volume) {
        this.#soundManager.ChangeVolume(volume);
    }

    CameraTilt(tilt) {
        if (!this.#camera) return;
        this.#camera.Tilt(tilt);
    }


    StopAnimation() {

        if (DEBUG) {
            console.info("stop animation");
            MyDebugger.PrintAllSamples();
            MyDebugger.Reset();
        }

        this.#animationStopped = true;

        this.#soundManager.SetGamePlayPaused();
    }

    ContinueAnimation() {
        if (DEBUG) {
            console.info("start animation");
        }
        this.#animationStopped = false;
        this.#soundManager.SetGamePlayContinued();
    }

    ChangePlayerPosition(position) {
        this.#player.vehicle.DropToPosition(position);
    }
}

