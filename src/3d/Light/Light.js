import * as THREE from 'three';


const CAMERA_VIEWPORT={
    left:-11,
    bottom:-.5,
    right: 11,
    top:14,
}

export class Light {

    #DIRECTIONAL_LIGHT_COORDINATES = [7, 8.7, 9];

    #lightsGroup;
    #target;

    #directionalLight
    #ambientLight

    constructor({ target }) {
        this.#target = target;

        this.#lightsGroup = new THREE.Group();

        this.#directionalLight = new THREE.DirectionalLight(0xffffff, 4);
        this.#directionalLight.target = this.#target;
        this.#directionalLight.position.set(...this.#DIRECTIONAL_LIGHT_COORDINATES);
        this.#directionalLight.shadow.camera.near = 0.1;
        this.#directionalLight.shadow.camera.far = 50;
        this.#directionalLight.shadow.camera.left = CAMERA_VIEWPORT.left+3;
        this.#directionalLight.shadow.camera.bottom = CAMERA_VIEWPORT.bottom-2;
        this.#directionalLight.shadow.camera.right = CAMERA_VIEWPORT.right+6;
        this.#directionalLight.shadow.camera.top = CAMERA_VIEWPORT.top;
        this.#directionalLight.shadow.mapSize.width = 1512;
        this.#directionalLight.shadow.mapSize.height = 1024;
        this.#directionalLight.shadow.camera.fov = 50;
        this.#directionalLight.shadow.bias=-0.003;
        this.#directionalLight.castShadow = true;
        
        this.#lightsGroup.add(this.#directionalLight);
        

        this.#ambientLight = new THREE.AmbientLight(0xffffff, 1)
        this.#lightsGroup.add(  this.#ambientLight); 
    }

    get lights() {
        return this.#lightsGroup
    }

    Animate() {
        this.#directionalLight.position.x = this.#target.position.x + this.#DIRECTIONAL_LIGHT_COORDINATES[0];

        this.#directionalLight.shadow.camera.updateProjectionMatrix();
    }
}