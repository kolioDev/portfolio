import { LAYOUT_ITEMS } from './layoutItems';

import { Road } from './LayoutItems/Road';
import { Forest } from './LayoutItems/Forest';
import { Field } from './LayoutItems/Field';
import { GrassGround } from './LayoutItems/GrassGround';
import { AsphaltGround } from './LayoutItems/AsphaltGround';
import { WaterGround } from './LayoutItems/WaterGround';
import { DirtGround } from './LayoutItems/DirtGround';

import { STORY_ITEMS_PROPS } from '../../utils/storyItems';


export const LAYOUT_ITEMS_PROPS = {
    //PURE LAYOUT
    [LAYOUT_ITEMS.field]: {
        gltf: Field,
        hasClickEvent: true,
        width: 2,
    },
    [LAYOUT_ITEMS.forest]: {
        gltf: Forest,
        hasClickEvent: true,
        width: 2,
    },
    [LAYOUT_ITEMS.road]: {
        gltf: Road,
        hasClickEvent: false,
        width: 1,
    },
    [LAYOUT_ITEMS.grass]: {
        gltf: GrassGround,
        hasClickEvent: false,
        width: 2,
    },
    [LAYOUT_ITEMS.dirt]: {
        gltf: DirtGround,
        hasClickEvent: false,
        width: 2,
    },
    [LAYOUT_ITEMS.asphalt]: {
        gltf: AsphaltGround,
        hasClickEvent: false,
        width: 1,
    },
    [LAYOUT_ITEMS.water]: {
        gltf: WaterGround,
        hasClickEvent: false,
        width: 1,
    },
    //STORY ITEMS
    [LAYOUT_ITEMS.pmg]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.pmg],
    [LAYOUT_ITEMS.eg]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.eg],
    [LAYOUT_ITEMS.webLoz]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.webLoz],
    [LAYOUT_ITEMS.pe6o]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.pe6o],
    [LAYOUT_ITEMS.referee]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.referee],
    [LAYOUT_ITEMS.physics]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.physics],
    [LAYOUT_ITEMS.uchase]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.uchase],
    [LAYOUT_ITEMS.graduation]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.graduation],
    [LAYOUT_ITEMS.tue]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.tue],
    [LAYOUT_ITEMS.finish]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.finish],
    [LAYOUT_ITEMS.mountainBike]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.mountainBike],
    [LAYOUT_ITEMS.bmxStatic]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.bmxStatic],
    [LAYOUT_ITEMS.train]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.train],
    [LAYOUT_ITEMS.mountain]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.mountain],
    [LAYOUT_ITEMS.paraglider]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.paraglider],
    [LAYOUT_ITEMS.finishFinal]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.finishFinal],
    [LAYOUT_ITEMS.uchaseServer]: STORY_ITEMS_PROPS[LAYOUT_ITEMS.uchaseServer],
}