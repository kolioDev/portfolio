import * as THREE from 'three';
import _ from 'lodash';

import { Road } from './LayoutItems/Road';
import { LAYOUT_ITEMS } from './layoutItems';
import { LAYOUT_ITEMS_PROPS } from './layoutItemsProps';
import { ClickHandler } from '../Managers/ClickHandler';

import { ENVS, STORY_ITEMS, STORY_ITEMS_PROPS } from '../../utils/storyItems';


const FOREST_BLOCKS = 2;
const FIELD_BLOCKS = 3;
const FOREST_BLOCKS_OFFSET = -12;
const FIELD_BLOCK_OFFSET = -9;
const FOREST_PROBABILITY = 0.5//1// 0.3;


const STORY_ITEM_KEY = "STORY_ITEM_KEY";
const GROUND_KEY = "GROUND_KEY";

export class LayoutBox {

    static #layoutItemsGltf = {}


    static boxWidth; //apx 6.544661521911621
    static initialBoxX;

    // array tracking where are layout boxes present 
    static #presentBoxes = {
        //[road uuid]:{simblified blocks}
    };

    #blocks = {
        [LAYOUT_ITEMS.road]: null,
        [LAYOUT_ITEMS.forest]: null,
        [LAYOUT_ITEMS.field]: null,
        [STORY_ITEM_KEY]: null,
        [GROUND_KEY]: null,
    }

    #position = {
        x: 0, y: 0, z: 0,
    };

    #scene;

    #clickHandler;

    #storyCalculationBox; 
    #checkCalculationBox;

    constructor({ xPosition, scene}) {
        this.#scene = scene;
        this.#position.x = xPosition;
        this.#clickHandler = new ClickHandler();

        this.#storyCalculationBox = new THREE.Box2();
        this.#checkCalculationBox = new THREE.Box2();

        if (!LayoutBox.initialBoxX) {
            LayoutBox.initialBoxX = xPosition + LayoutBox.boxWidth;
        }

        this.#createBox();
    }

    destructor() {
        this.#removeBox();
    }

    #createBox() {
        this.#createStoryEnvironment();
        this.#createBasicEnvironment();
        this.#storeBoxInfo();

    }

    #createBasicEnvironment() {
        //Creates basic environment blocks - road, trees, fields

        //Add road block
        this.#createMesh({
            x: this.#position.x,
            itemKey: LAYOUT_ITEMS.road
        })

        this.#createNatureBox();

    }

    #createNatureBox() {
        const blocksAbove = this.#blocksAbove(this.#position.x);
        const blocksAboveNext = this.#blocksAbove(this.#position.x + LayoutBox.boxWidth)

        //Add forest  or field blocks
        if (!blocksAbove.includes(LAYOUT_ITEMS.field)
            &&
            !blocksAbove.includes(LAYOUT_ITEMS.forest)
        ) {

            const isForest = FOREST_PROBABILITY > Math.random();

            const x =
                blocksAboveNext.includes(LAYOUT_ITEMS.field) ||
                    blocksAboveNext.includes(LAYOUT_ITEMS.forest) ||
                    this.#isOccupidedByStoryEnvironment({ leftX: this.#position.x, rightX: this.#position.x + LayoutBox.boxWidth })
                    ?
                    this.#position.x - LayoutBox.boxWidth :
                    this.#position.x



            if (!this.#isOccupidedByStoryEnvironment({ leftX: x, rightX: x + LayoutBox.boxWidth })
            ) {
                this.#placeNatureMesh({ isForest, x })
            }
        }

        // Create ground required by story items
        const storyAtPosition = this.#storyItemAtPosition({
            leftX: this.position.x,
            rightX: this.position.x + 2
        });
        if (
            storyAtPosition &&
            [ENVS.asphalt, ENVS.water].includes(STORY_ITEMS_PROPS[storyAtPosition].env) &&
            !blocksAbove.includes(LAYOUT_ITEMS.field) &&
            !blocksAbove.includes(LAYOUT_ITEMS.forest)
        ) {
            this.#createMesh({
                x: this.#position.x,
                z: -7,
                itemKey: STORY_ITEMS_PROPS[storyAtPosition].env,
                localKey: GROUND_KEY
            })
        }


    }

    #placeNatureMesh({ isForest, x }) {
        const localKey = isForest ? LAYOUT_ITEMS.forest : LAYOUT_ITEMS.field;
        const numberOfBlocks = isForest ? FOREST_BLOCKS : FIELD_BLOCKS;
        const numberOfRows = isForest ? 1 : 2;
        const blockOffset = isForest ? FOREST_BLOCKS_OFFSET : FIELD_BLOCK_OFFSET

        if (this.#blocks[localKey]) {
            // something strange is going on in here
            this.#scene.remove(this.#blocks[localKey]);
        }

        this.#blocks[localKey] = new THREE.Group();
        for (let r = 0; r < numberOfRows; r++) { //Add 2 rows of  FIELD_BLOCKS columns
            for (let i = 0; i < numberOfBlocks; i++) {

                this.#createMesh({
                    x: LayoutBox.boxWidth * r,
                    z: LayoutBox.#layoutItemsGltf[localKey].scene.position.z + i * blockOffset,
                    itemKey: localKey,
                    group: this.#blocks[localKey],
                })

            }
        }
        this.#blocks[localKey].position.x = x
        this.#scene.add(this.#blocks[localKey]);



        //Using ground group allows the ground to have the same x as everything else
        this.#blocks[GROUND_KEY] = new THREE.Group();
        this.#createMesh({
            x: 1 / 2 * LayoutBox.boxWidth,
            itemKey: isForest ? LAYOUT_ITEMS.grass : LAYOUT_ITEMS.dirt,
            group: this.#blocks[GROUND_KEY]
        })

        this.#blocks[GROUND_KEY].position.x = x
        this.#scene.add(this.#blocks[GROUND_KEY]);
    }

    //needs to be drastically rewriten with the new logic keeping track of blocks
    #createStoryEnvironment() {

        const storyItem = this.#storyItemAtPosition({
            leftX: this.position.x,
            rightX: this.position.x + 2
        })
        if (!storyItem) {
            return;
        }

        if (
            !this.#blocksAbove(this.#position.x).includes(storyItem)
            && !this.#blocks[STORY_ITEM_KEY]
            && !_.find(LayoutBox.#presentBoxes, b => b[STORY_ITEM_KEY]?.meta.itemKey === storyItem)
        ) {

            const itemProps = STORY_ITEMS_PROPS[storyItem];
            const x = (itemProps.position * LayoutBox.boxWidth)
                + LayoutBox.initialBoxX;


            const creationPayload = {
                x: x + itemProps.offset,
                itemKey: storyItem,
                localKey: STORY_ITEM_KEY
            }

            if (itemProps.animated) {
                this.#placeAnimatedStory(creationPayload);
            }else{
                this.#createMesh(creationPayload);
            }

            if (itemProps.env === ENVS.forrest) {
                this.#placeNatureMesh({
                    isForest: true,
                    x: x + LayoutBox.boxWidth,
                })
            }
        }

    }

    #storeBoxInfo() {
        let id;
        const simplifiedInfo = _.mapValues(this.#blocks, (block, key) => {
            if (!block) return block;

            const meta = {};

            if (key === LAYOUT_ITEMS.road) {
                id = block.uuid;
            }

            if (key === STORY_ITEM_KEY) {
                meta.itemKey = block.itemKey;
            }

            return {
                x: block.position.x,
                meta
            };
        });

        LayoutBox.#presentBoxes[id] = simplifiedInfo;
    }

    #storyItemAtPosition({ leftX, rightX }) {
        for (const k in STORY_ITEMS) {
            const item = STORY_ITEMS[k];

            const xPosition = (STORY_ITEMS_PROPS[item].position) * LayoutBox.boxWidth;

            this.#storyCalculationBox.min.x = xPosition;
            this.#storyCalculationBox.min.y = 1;
            this.#storyCalculationBox.max.x = xPosition + STORY_ITEMS_PROPS[item].width  * LayoutBox.boxWidth;
            this.#storyCalculationBox.max.y = 1;

            this.#checkCalculationBox.min.x = leftX;
            this.#checkCalculationBox.min.y = 1;
            this.#checkCalculationBox.max.x = rightX-1;
            this.#checkCalculationBox.max.y = 1;

            if (this.#checkCalculationBox.intersectsBox(this.#storyCalculationBox)) {
                return item;
            }
        }
        return null;
    }

    #isOccupidedByStoryEnvironment({ leftX, rightX }) {
        const item = this.#storyItemAtPosition({ leftX, rightX });
        if (item) {
            return true;
            // return STORY_ITEMS_PROPS[item].env !== ENVS.default;
        }
        return false;
    }

    #removeBox() {
        this.#removeBoxInfo();

        for (const key in this.#blocks) {
            const block = this.#blocks[key];

            if (block === null) {
                continue;
            }

            if (LAYOUT_ITEMS_PROPS[key]?.hasClickEvent) {
                this.#clickHandler.DeleteObject(block);
            }

            if (LAYOUT_ITEMS_PROPS[key]?.animated) {
                this.#scene.remove(block.scene);
            } else {
                this.#scene.remove(block);
            }


        }

    }

    #removeBoxInfo() {
        delete LayoutBox.#presentBoxes[
            this.#blocks[LAYOUT_ITEMS.road].uuid
        ];
    }

    #createMesh({ x, z = null, itemKey, group, localKey = itemKey }) {

        if (this.#blocks[localKey] && !group) {
            return;
        }

        const gltf = LayoutBox.#layoutItemsGltf[itemKey]

        const mesh = gltf.scene.clone();
        mesh.itemKey = itemKey;
        mesh.position.x = x;
        if (z !== null) {
            mesh.position.z = z;
        }

        if (LAYOUT_ITEMS_PROPS[itemKey].hasClickEvent) {
            this.#clickHandler.AddObject({
                object: mesh,
                key: itemKey
            });
        }

        if (group) {
            group.add(mesh);
            return mesh;
        }

        this.#scene.add(mesh);
        this.#blocks[localKey] = mesh;

        return mesh;
    }

    #placeAnimatedStory({ x, itemKey, localKey }) {
        if (this.#blocks[localKey]) {
            return;
        }

        const gltf = LayoutBox.#layoutItemsGltf[itemKey]
        gltf.scene.position.x = x;
        this.#scene.add(gltf.scene);
        //Note that for animated story the gltf is added, not just the mesh
        gltf.position = gltf.scene.position
        gltf.itemKey = itemKey;
        this.#blocks[localKey] = gltf;
    }


    #blocksAbove(x) {
        const blockAbove = [];

        _.forEach(LayoutBox.#presentBoxes, box => {
            _.forEach(box, (block, key) => {
                if (!block) return;

                if (key === GROUND_KEY) {
                    key = LAYOUT_ITEMS.grass
                }
                if (key === STORY_ITEM_KEY) {
                    key = block.meta.itemKey
                }

                const lowerBoundery = block.x;
                const upperBoundery = block.x + LayoutBox.boxWidth * LAYOUT_ITEMS_PROPS[key].width;

                if (x >= lowerBoundery && x < upperBoundery) {
                    blockAbove.push(key);
                }
            })
        });

        return blockAbove;
    }


    get position() {
        return this.#position;
    }

    Recalculate() {
        //If for some reason a road box does not have needed attributes above 
        //(nature block, story box) - add them
        // return;
        this.#createNatureBox()
        this.#createStoryEnvironment()

        this.#storeBoxInfo();
    }

    Animate(deltaT) {
        //If the story item of the layout box has animated=true
        const storyItem = this.#blocks[STORY_ITEM_KEY];
        if (storyItem && LAYOUT_ITEMS_PROPS[storyItem.itemKey].animated) {
            storyItem.Animate(deltaT)
        }
    }


    static async LoadModels({player}) {
        this.#layoutItemsGltf[LAYOUT_ITEMS.road] = new Road();
        await this.#layoutItemsGltf[LAYOUT_ITEMS.road].Load();

        const roadBox = new THREE.Box3().setFromObject(this.#layoutItemsGltf[LAYOUT_ITEMS.road].scene);
        this.boxWidth = Math.abs(roadBox.min.x) + Math.abs(roadBox.max.x);

        const promises = [];


        for (const key in LAYOUT_ITEMS) {
            const item = LAYOUT_ITEMS[key];
            const itemProps = LAYOUT_ITEMS_PROPS[item];
            
            if ([LAYOUT_ITEMS.grass, LAYOUT_ITEMS.asphalt, LAYOUT_ITEMS.dirt, LAYOUT_ITEMS.water,].includes(item)) {
                this.#layoutItemsGltf[item] = new itemProps.gltf({
                    width: this.boxWidth * itemProps.width,
                    height: 75,
                });
            } else {
                this.#layoutItemsGltf[item] = new itemProps.gltf();
            }

            
            const payload = itemProps.animated ? {player} : null; 

            promises.push(this.#layoutItemsGltf[item].Load(payload))
        }


        return Promise.all(promises);

    }
}
