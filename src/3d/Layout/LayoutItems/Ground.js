import * as THREE from 'three';


export class Ground {

    #ground
    #width
    #height

    _color = 0x0;

    constructor({ width, height }) {
        this.#height = height;
        this.#width = width;
    }

    Load() {
        const geometry = new THREE.PlaneGeometry(this.#width, this.#height);
        const material = new THREE.MeshStandardMaterial({
            color: this._color,
            side: THREE.FrontSide
        });
        this.#ground = new THREE.Mesh(geometry, material);

        this.#ground.rotateX(-Math.PI / 2);
        this.#ground.receiveShadow = true;
        this.#ground.castShadow = true;

        this.#ground.position.z = -32;
        this.#ground.position.y = -.23;
    }

    get scene() {
        return this.#ground;
    }
}