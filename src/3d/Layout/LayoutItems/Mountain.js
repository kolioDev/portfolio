import { GLTF } from '../../Gltf.js';


export class Mountain extends GLTF {
    _gltf;
    _filename = 'mountain.glb';
    
    _z = -6;
    _y = -.14;

    _castShadow = true;
    _receiveShadow = true;

}