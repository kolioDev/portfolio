import { GLTF } from '../../Gltf.js';


export class Uchase extends GLTF {
    _gltf;
    _filename = 'uchase.glb';

    _y = -.25;

    _castShadow = true;
    _receiveShadow = true;
}