import { GLTF } from '../../Gltf.js';


export class Train extends GLTF {
    _gltf;
    _filename = 'train.glb';

    _z = -3;
    _y = -.14;

    _castShadow = true;
    _receiveShadow = true;    
}