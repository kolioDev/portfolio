import { GLTF } from '../../Gltf.js';


export class BMXStatic extends GLTF {
    _gltf;
    _filename = 'bmx_static.glb';

    _z = -8;
    _y = -.2;

    _castShadow = true;
    _receiveShadow = true;

    
}