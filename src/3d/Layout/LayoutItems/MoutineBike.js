import { GLTF } from '../../Gltf.js';


export class MoutineBike extends GLTF {
    _gltf;
    _filename = 'moutine_bike.glb';
    
    _z = -4;
    _y = -.2;

    _castShadow = true;
}