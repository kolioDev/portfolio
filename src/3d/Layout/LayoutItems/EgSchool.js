import { GLTF } from '../../Gltf.js';


export class EGSchool extends GLTF {
    _gltf;
    _filename = 'eg.glb';

    _z = -10;
    _y = -.3;

    _castShadow = true;
    _receiveShadow = true;
    
}