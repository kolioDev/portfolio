import { GLTF } from '../../Gltf.js';


export class Pe6o extends GLTF {
    _gltf;
    _filename = 'pe6o.glb';

    _z = -.6;
    _y = -.85;

    _castShadow = true;    
}