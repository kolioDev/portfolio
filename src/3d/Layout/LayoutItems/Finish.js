import * as THREE from 'three';
import { GLTF } from '../../Gltf.js';
import { SoundManager } from "../../Managers/SoundManager";

const TEAR_ANIMATION = "Tear";
const PLAYER_PROXIMITY_FOR_ANIMATION = 1;

const CLOTH_RIP_SOUND = "cloth_rip";
export class Finish extends GLTF {
    _gltf;
    _filename = 'finish.glb';


    _y = -.24;

    _castShadow = true;


    #animationMixer;
    #animationActions = [];
    #player;

    #animationPlayed = false;
    #soundManager;

    constructor() {
        super();
        this.#soundManager = new SoundManager();
        this.#soundManager.RegisterSound(CLOTH_RIP_SOUND, './sounds/cloth_rip.mp3');
    }


    async Load({ player }) {
        await super.Load();

        this.#player = player;

        this.#animationMixer = new THREE.AnimationMixer(this._gltf.scene);

        // Clip all animations
        this._gltf.animations.forEach((clip) => {
            this.#animationActions[clip.name] = this.#animationMixer.clipAction(clip);
        });

        this.#animationActions[TEAR_ANIMATION].clampWhenFinished = true;
        this.#animationActions[TEAR_ANIMATION].setLoop(THREE.LoopOnce);
    }


    Animate(deltaT) {
        if (!this.#animationPlayed) {
            const proximity = Math.abs(this._gltf.scene.position.x - this.#player.vehicle.position.x)
            if (proximity < PLAYER_PROXIMITY_FOR_ANIMATION) {
                this.#animationActions[TEAR_ANIMATION].play();
                this.#soundManager.PlaySound(CLOTH_RIP_SOUND);
                this.#animationPlayed = true;
            }
        }

        this.#animationMixer.update(deltaT / 1000); //time in seconds

    }



}