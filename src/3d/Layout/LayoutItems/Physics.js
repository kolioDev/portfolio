import { GLTF } from '../../Gltf.js';


export class Physics extends GLTF {
    _gltf;
    _filename = 'physics.glb';

    _z = -4;
    _y = -.25;

    _castShadow = true;
    _receiveShadow = true;
}