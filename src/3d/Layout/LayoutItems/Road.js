import { GLTF } from '../../Gltf.js';


export class Road extends GLTF {
    _gltf;
    _filename = 'road_wdirt.glb';

    _z = -0.1;
    _y = -.23;

    _receiveShadow = true;
}