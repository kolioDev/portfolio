// import { DomEvent } from '../../domEvent.js';
import { GLTF } from '../../Gltf.js';


export class Field extends GLTF {
    _gltf;
    _filename = 'field.glb';

    _z = -6.7;
    _y = -.3;

    _castShadow = true;
    _receiveShadow = true;

}
