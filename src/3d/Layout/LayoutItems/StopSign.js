import { GLTF } from '../../Gltf.js';


export class StopSign extends GLTF {
    _gltf;
    _filename = 'stop_anim.glb';

    _y = -.25;

    _castShadow = true;
    _receiveShadow = true;    
}