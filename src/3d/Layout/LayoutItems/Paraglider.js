import { GLTF } from '../../Gltf.js';


export class Paraglider extends GLTF {
    _gltf;
    _filename = 'paraglider.glb';

    _z = -10;
    _y = .5;

    _castShadow = true;
    
}