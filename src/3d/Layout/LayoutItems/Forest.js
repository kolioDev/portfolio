import { GLTF } from '../../Gltf.js';


export class Forest extends GLTF {
    _gltf;
    _filename = 'forest.glb';

    _z = -7.1;
    _y = -.7;

    _castShadow = true;
    _receiveShadow = true;    
}