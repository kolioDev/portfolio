import { GLTF } from '../../Gltf.js';


export class PMGSchool extends GLTF {
    _gltf;
    _filename = 'school_pmg.glb';

    _z = -10;
    _y = -.3;

    _castShadow = true;
    _receiveShadow = true;
}