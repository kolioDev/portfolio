import { GLTF } from '../../Gltf.js';


export class Graduation extends GLTF {
    _gltf;
    _filename = 'graduation.glb';

    _z = -4;
    _y = -.27;

    _castShadow = true;
    _receiveShadow = true;
}