import { GLTF } from '../../Gltf.js';


export class Webloz extends GLTF {
    _gltf;
    _filename = 'webloz.glb';

    _z = -2;
    _y = 1;

    _castShadow = true;
}