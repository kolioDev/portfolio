import * as THREE from 'three';
import _ from 'lodash';
import { Vector2 } from 'three';

import { LayoutBox } from './LayoutBox';


const NUMBER_OF_INITIAL_BOXES = 2;

export class Layout {
    #nVisibleLayoutBoxes;
    #scene;

    #layoutBoxes = [];

    #fieldOfView;
    #player;

    #currentPovBox;

    constructor({ scene, fieldOfView, player }) {
        this.#scene = scene;
        this.#fieldOfView = fieldOfView;
        this.#player = player;

        this.#currentPovBox = new THREE.Box3(
            new Vector2(0, 0),
            new Vector2(0, 0)
        );
    }

    async Init() {
        await LayoutBox.LoadModels( {player:this.#player});
        this.PlaceLayoutBoxesInitial();
    }

    //Initial placement;
    PlaceLayoutBoxesInitial() {
        this.#updateNVisibleLayoutBoxes();

        // for (let i = 0; i < NUMBER_OF_INITIAL_BOXES ; i++) {
        for (let i = 0; i > -NUMBER_OF_INITIAL_BOXES ; i--) {
            this.#layoutBoxes[i] =
                new LayoutBox({
                    xPosition: i * LayoutBox.boxWidth + this.#fieldOfView.min.x,
                    scene: this.#scene
                })
        }
    }

    UpdateFieldOfView(newFieldOfView) {

        this.#updateNVisibleLayoutBoxes(newFieldOfView);

        const coveredField = this.#calCurrentPov()

        if (!coveredField.containsBox(newFieldOfView)) {
            const rightDiff = newFieldOfView.max.x - coveredField.max.x;
            const leftDiff = -(newFieldOfView.min.x - coveredField.min.x);


             if (rightDiff > 0) {
                let roadsToAdd = rightDiff / LayoutBox.boxWidth;
                roadsToAdd = Math.ceil(roadsToAdd);
                for (let i = 0; i < roadsToAdd; i++) {

                    this.#placeLayoutBox(
                        coveredField.max.x + (LayoutBox.boxWidth * i),
                        true)
                }
            }

            if (leftDiff > 0) {
                let roadsToAdd = leftDiff / LayoutBox.boxWidth;
                roadsToAdd = Math.ceil(roadsToAdd);
                for (let i = 0; i < roadsToAdd; i++) {
                    this.#placeLayoutBox(
                        coveredField.min.x - (LayoutBox.boxWidth * (i + 1)),
                        false)
                }
            }



        }


        this.#fieldOfView = this.#calCurrentPov();
    }

    #placeLayoutBox(x, toTheRight = false) {

        const layoutBox = new LayoutBox({
            xPosition: x, scene: this.#scene
        });


        if (toTheRight) {
            this.#layoutBoxes.push(layoutBox);
        } else {
            this.#layoutBoxes.unshift(layoutBox);
        }

    }


    #updateNVisibleLayoutBoxes(newFieldOfView = null) {

        let fov = this.#fieldOfView
        if (newFieldOfView) {
            fov = newFieldOfView;
        }

        this.#nVisibleLayoutBoxes = fov.getSize(new THREE.Vector2()).x / LayoutBox.boxWidth;
        this.#nVisibleLayoutBoxes = Math.ceil(this.#nVisibleLayoutBoxes);

        // Remove unnecessary roads if fieldOfView shrinks (or moves)
        if (this.#nVisibleLayoutBoxes < this.#layoutBoxes.length - 2) {
            const currentPovCenter = this.#calCurrentPov().getCenter(new THREE.Vector2()).x
            const newPovCenter = fov.getCenter(new THREE.Vector2()).x

            const roadsToRemove = this.#layoutBoxes.length - this.#nVisibleLayoutBoxes - 2;
            for (var i = 0; i < roadsToRemove; i++) {
                const left = currentPovCenter < newPovCenter;
                const layOutBoxToDelete = left ? this.#layoutBoxes.shift() : this.#layoutBoxes.pop();

                this.#removeLayoutBox(layOutBoxToDelete);
            }
        }
    }

    //Calculates the area currently covered by layout boxes
    #calCurrentPov() {
        if(this.#layoutBoxes.length === 0 ) return this.#currentPovBox

        this.#currentPovBox.min =
            _.first(this.#layoutBoxes).position;
        this.#currentPovBox.max.x =  _.last(this.#layoutBoxes).position.x + LayoutBox.boxWidth,
        this.#currentPovBox.max.y =  _.last(this.#layoutBoxes).position.y

        return this.#currentPovBox;
    }


    #removeLayoutBox(layoutBox) {
        delete (layoutBox.destructor());

        //Loops through this.#layoutBoxes starting from the middle and going evenly
        //to right and then left
        //eg [1,2,3,4,5] will be looped as 3,4,2,5,1
        let i = 0;
        while (true) {
            const index = Math.ceil(this.#layoutBoxes.length / 2) + i - 1;
            if (index < 0 || index >= this.#layoutBoxes.length) {
                break;
            }

            this.#layoutBoxes[index].Recalculate();


            i = i <= 0 ? -1 * i + 1 : -1 * i;
        }

    }

    Animate({ fieldOfView, deltaT }) {
        this.UpdateFieldOfView(fieldOfView);
        for(let i=0; i<this.#layoutBoxes.length;i++){
            this.#layoutBoxes[i].Animate(deltaT)
        }
    }
}