export const LAYOUT_ITEMS = {
    //PURE LAYOUT
    field: 'FIELD',
    forest: 'FOREST',
    grass: 'GRASS',
    dirt: 'DIRT',
    asphalt: 'ASPHALT',
    water: 'WATER',
    road: 'ROAD',

    //STORY ITEMS
    pmg: 'PMG',
    eg: 'EG',
    webLoz: 'WEBLOZ',
    pe6o: 'PE6O',
    physics: 'PHYSICS',
    referee: 'REFEREE',
    uchase: 'UCHASE',
    graduation: 'GRADUATION',
    uchaseServer: 'UCHASE_SERVER',
    tue:'TUE',
    finish:'FINISH',
    mountainBike: 'MOUTINE_BIKE',
    bmxStatic: 'BMX_STATIC',
    train: 'TRAIN',
    mountain: 'MOUNTINE',
    paraglider: 'PARAGLIDER',
    finishFinal: 'FINISH_FINAL',
}


