import * as THREE from 'three';
import _ from 'lodash';

export class ClickHandler { //Singleton

    static #instance = null;

    #raycaster;
    #mouse;
    #domElement;
    #camera;

    #objects = []

    constructor(params) {
        if (!ClickHandler.#instance) {
            const { domElement, camera } = params;
            this.#domElement = domElement;
            this.#camera = camera;
            this.#raycaster = new THREE.Raycaster();
            this.#mouse = new THREE.Vector2();
            ClickHandler.#instance = this;
        }
        return ClickHandler.#instance;
    }

    onClick(event) {
        var rect = this.#domElement.getBoundingClientRect();
        this.#mouse.x = ((event.clientX - rect.left) / (rect.width - rect.left)) * 2 - 1;
        this.#mouse.y = - ((event.clientY - rect.top) / (rect.bottom - rect.top)) * 2 + 1;

        return this.#intersects();
    
    }

    #intersects() {

        this.#raycaster.setFromCamera(this.#mouse, this.#camera);

        for (let i = 0; i < this.#objects.length; i++) {
            const intersects = this.#raycaster.intersectObject(this.#objects[i]);
            if (intersects.length > 0) {
                return this.#objects[i].clickHandlerKey;
            }
        }

        return false;
    }

    AddObject({ object, key }) {
        object.traverse(child => {
            if (child.isMesh) {
                child.clickHandlerKey = key;
                this.#objects.push(child);
            }
        });
    }

    DeleteObject(object) {
        const uuids = [];
        object.traverse(child => {
            if (child.isMesh) {
                uuids.push(child.uuid)
            }
        });

        this.#objects = _.reject(this.#objects, o => uuids.includes(o.uuid));
    }
}

