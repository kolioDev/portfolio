import _ from 'lodash';

export class EmptyDebugger {
    LogSampleStart = () => { }
    LogSampleEnd = () => { }
    GetSample = () => { }
    PrintAllSamples = () => { }
    Reset = () => { }
}

export class Debugger {

    _targets = [];


    _numberOfCycles = []
    _timePassed = [];
    _longestCycle = [];
    _frameOfLongestCycle = [];

    _deltaT = []


    LogSampleStart(...targets) {


        for (const target of targets) {
            let index = this._targets.findIndex(e => e === target);
            if (index === -1) {
                index = this._targets.length;
                this._targets[index] = target;
                this._numberOfCycles[index] = 0;
                this._timePassed[index] = 0;
                this._longestCycle[index] = 0;
                this._deltaT[index] = 0;
            }

            this._deltaT[index] = performance.now();
        }


    }

    LogSampleEnd(...targets) {
        for (const target of targets) {
            const index = this._targets.findIndex(e => e === target);
            if (index === -1) {
                throw new Error(`Target "${target}" don't have initial sample`);
            }

            const cycleLength = performance.now() - this._deltaT[index];


            this._timePassed[index] += cycleLength;
            this._numberOfCycles[index]++;

            if (this._longestCycle[index] < cycleLength) {
                this._longestCycle[index] = cycleLength;
                this._frameOfLongestCycle[index] = this._numberOfCycles[index];
            }

            this._deltaT[index] = 0;
        }



    }


    GetSample(target) {
        const index = this._targets.findIndex(e => e === target);
        if (index === -1) {
            throw new Error(`Target "${target}" don't have initial sample`);
        }

        const avgT = this._timePassed[index] / this._numberOfCycles[index];
        const avgFPS = 1000 / avgT;
        const minFPS = 1000 / this._longestCycle[index];


        return {
            avgFPS, minFPS, avgT, maxT: this._longestCycle[index],
            maxTFrame: this._frameOfLongestCycle[index],
        };
    }

    PrintAllSamples() {
        const data = {};

        for (let i = 0; i < this._targets.length; i++) {
            const target = this._targets[i];
            data[target] = this.GetSample(target);
        }

        console.table(data);
    }

    Reset() {
        this._targets = [];

    }
}