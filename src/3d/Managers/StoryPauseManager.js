import * as THREE from 'three';
import { StopSign } from "../Layout/LayoutItems/StopSign";
import { STORY_ITEMS_PROPS, STORY_ITEMS } from "../../utils/storyItems";
import { SoundManager } from './SoundManager';

//Approximate distance before the stop at witch the vehicle should stop
const DIST_TO_STOP = 3;
//The distance between the player and the stop sign 
// withing witch is considered the player is on the stops position
const STOP_POSITION_THRESHOLD = 0.2

const SIGN_DROP_SOUND = 'sign_drop';

const NUMBER_OF_DELAYED_SOUND_FRAMES = 5;
export class StoryPauseManager { //Singleton

    static #instance = null;

    #player;
    #scene;
    #stopSignGltf;

    #shouldStopForward = false;  //Whether vehicle throttle should be released 
    #shouldStopBackward = false;

    #triggerThrottleRelease = false;
    #throttleReleaseTriggered = false;

    #triggerMessage = null;
    #messageTriggered = false;

    #trafficSignsCurrentlyAnimated = [];

    //{position:int, visited:bool, key:string, mesh:THREE.Mesh}
    #stops = [];

    //{ mixer, clip, animAction }
    #animations = [];

    #prevPlayerSpeed = 0;

    #framesToDelayedSoundTrigger = +Infinity;

    #soundManager;

    constructor(params) {
        if (!StoryPauseManager.#instance) {
            const { player, scene } = params;

            this.#player = player;
            this.#scene = scene;

            this.#soundManager = new SoundManager();
            this.#soundManager.RegisterSound(SIGN_DROP_SOUND, './sounds/stop_sign_drop.mp3');


            StoryPauseManager.#instance = this;
        }
        return StoryPauseManager.#instance;
    }


    async Load() {

        if (!this.#stopSignGltf) {
            this.#stopSignGltf = new StopSign();
            await this.#stopSignGltf.Load();
            this.#placeStopSigns();
        }
    }


    #placeStopSigns() {
        for (const k in STORY_ITEMS) {
            const item = STORY_ITEMS[k];
            const params = STORY_ITEMS_PROPS[item];
            if (params.pauseCoordinate || params.pauseCoordinate === 0) {
                const mesh = this.#createMesh(params.pauseCoordinate);

                this.#stops.push({
                    position: params.pauseCoordinate,
                    visited: false,
                    key: item,
                    mesh
                })

                this.#animations.push(this.#clipAnimation(mesh));
            }
        }
    }


    #createMesh(x) {
        const mesh = this.#stopSignGltf.scene.clone();
        mesh.position.x = x;
        this.#scene.add(mesh);
        return mesh;
    }

    #clipAnimation(mesh) {
        const mixer = new THREE.AnimationMixer(mesh);
        const clip = this.#stopSignGltf._gltf.animations[0];
        const animAction = mixer.clipAction(clip);
        animAction.clampWhenFinished = true;
        animAction.setLoop(THREE.LoopOnce);

        return { mixer, clip, animAction }
    }


    //returns signed distance to next stop sign
    #nearestStop() {
        const currentX = this.#player.vehicle.position.x;
        let minDist = Infinity;
        let stopX = 0;
        let stopKey = null;
        let stopIndex = null;

        for (const [index, stop] of this.#stops.entries()) {
            if (stop.visited) continue;

            const dist = stop.position - currentX
            if (Math.abs(dist) < Math.abs(minDist)) {
                minDist = dist
                stopX = stop.position
                stopKey = stop.key
                stopIndex = index
            }
        }

        return { dist: minDist, x: stopX, key: stopKey, index: stopIndex };
    }

    #calculateShouldStop() {

        const { dist: distToStop, x: stopX } = this.#nearestStop();
        const v0 = this.#player.vehicle.speed;
        const a = Math.abs(this.#player.vehicle.deceleration);
        const criticalDist = this.#player.vehicle.velocityMultiplier * (v0 ** 2) / (2 * a);
        const x = this.#player.vehicle.position.x;


        //This might need tweaking
        const shouldStopForward = v0 > 0 && (criticalDist + DIST_TO_STOP) >= distToStop && x < stopX - STOP_POSITION_THRESHOLD;
        const shouldStopBackward = v0 < 0 && (criticalDist - DIST_TO_STOP) <= distToStop && x > stopX + STOP_POSITION_THRESHOLD;
        
        if (shouldStopForward || shouldStopBackward) {
            //Ensures single throttle release trigger
            if (!this.#throttleReleaseTriggered) {
                this.#triggerThrottleRelease = true;
                this.#throttleReleaseTriggered = true;
            } else {
                this.#triggerThrottleRelease = false;
            }

            this.#shouldStopBackward = shouldStopBackward;
            this.#shouldStopForward = shouldStopForward;

            return;
        }

        this.#shouldStopBackward = false;
        this.#shouldStopForward = false;

        this.#throttleReleaseTriggered = false;
        this.#triggerThrottleRelease = false;
    }

    #calculateShowMessage() {
        const speed = this.#player.vehicle.speed;
        const { dist: distToStop, key, index } = this.#nearestStop();

        if (
            (this.#prevPlayerSpeed > 0 && speed == 0 && distToStop > 0 && distToStop <= (DIST_TO_STOP + STOP_POSITION_THRESHOLD))
            || (this.#prevPlayerSpeed < 0 && speed == 0 && Math.abs(distToStop) <= (DIST_TO_STOP + STOP_POSITION_THRESHOLD))
        ) {

            if (!this.#messageTriggered) {
                this.#triggerMessage = { key, index };
                this.#messageTriggered = true;
                return;
            }
            this.#triggerMessage = null
            return;
        }

        this.#prevPlayerSpeed = speed;

        this.#messageTriggered = false;
        this.#triggerMessage = null
    }

    #applyPlayerIsDroppingToPositionPreventions() {
        if (this.#player.vehicle.isDroppingToPosition) {
            this.#shouldStopBackward, this.#shouldStopForward = false, false
            this.#throttleReleaseTriggered = false;
            this.#triggerThrottleRelease = false;
            this.#messageTriggered = false;
            this.#triggerMessage = null
            this.#prevPlayerSpeed = 0;
        }
    }

    #markStopAsVisited(index) {
        this.#stops[index].visited = true;
    }

    #triggerStopSignAnimation(stopIndex) {
        this.#trafficSignsCurrentlyAnimated.push(stopIndex);
        this.#animations[stopIndex].animAction.play();

        //Trigger sound after the game continues
        this.#framesToDelayedSoundTrigger = NUMBER_OF_DELAYED_SOUND_FRAMES;

        this.#animations[stopIndex].mixer.addEventListener('finished', () => {
            this.#trafficSignsCurrentlyAnimated =
                this.#trafficSignsCurrentlyAnimated.filter(e => e != stopIndex)
        });
    }

    #animateSigns(deltaT) {
        for (const index of this.#trafficSignsCurrentlyAnimated) {
            this.#animations[index].mixer.update(deltaT / 1000);
        }
    }


    get shouldStopBackward() {
        return this.#shouldStopBackward;
    }

    get shouldStopForward() {
        return this.#shouldStopForward;
    }

    Animate(deltaT) {
        this.#calculateShouldStop();
        this.#calculateShowMessage();
        this.#applyPlayerIsDroppingToPositionPreventions();

        if (this.#triggerMessage) {
            this.#markStopAsVisited(this.#triggerMessage.index);
            this.#triggerStopSignAnimation(this.#triggerMessage.index);
        }

        this.#animateSigns(deltaT);


        if (this.#framesToDelayedSoundTrigger === 1) {
            this.#soundManager.PlaySoundDelayed(SIGN_DROP_SOUND, [950, 815]);
            this.#framesToDelayedSoundTrigger = Infinity
        }
        this.#framesToDelayedSoundTrigger -= 1


        return {
            releaseThrottle: this.#triggerThrottleRelease,
            showMessage: this.#triggerMessage ? this.#triggerMessage.key : false
        }
    }

}