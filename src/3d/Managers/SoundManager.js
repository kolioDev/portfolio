import { Howl, Howler } from 'howler';
import { INITIAL_VOLUME } from '../../utils/volume';

//singleton
export class SoundManager {

    static #instance = null;

    #sounds = {};
    #playing = [];
    #activeTimeouts = {};

    #gamePlayPaused = false;

    constructor() {
        if (!SoundManager.#instance) {
            Howler.volume(INITIAL_VOLUME);
        }
        return SoundManager.#instance;
    }


    RegisterSound(soundName, soundPath, { repeat = false, volume = 1 } = {}) {
        this.#sounds[soundName] = new Howl({
            src: [soundPath],
            volume: 1,
            loop: repeat,
            volume,
            html5: false,
        });
    }

    PlaySound(soundName) {
        const repeat = this.#sounds[soundName].loop();

        if (repeat && this.#playing.includes(soundName)) {
            return;
        }

        if (repeat) {
            this.#playing.push(soundName);
        }

        this.#sounds[soundName].play();
    }

    //Recursively executed
    //Can accept bot int and array of int for delay
    //If array with length n  is passed in will fire n delayed sound.play
    PlaySoundDelayed(soundName, delay) {
        if (Array.isArray(delay) && delay.length === 0) {
            return;
        }

        if (soundName in this.#activeTimeouts) {
            clearTimeout(this.#activeTimeouts[soundName]);
            //remove the timeout from the list
            delete this.#activeTimeouts[soundName];
        }

        if (!Array.isArray(delay)) {
            delay = [delay]
        }

        this.#activeTimeouts[soundName] = setTimeout(() => {
            this.PlaySound(soundName);
            this.PlaySoundDelayed(soundName, [...delay].splice(1, delay.length));
        }, delay[0]);
    }


    PauseSound(soundName) {
        this.#playing.splice(this.#playing.indexOf(soundName), 1);
        this.#sounds[soundName].pause();
    }



    SetRate(soundName, rate) {
        this.#sounds[soundName].rate(rate);
    }


    SetGamePlayPaused() {
        this.#gamePlayPaused = true;
        this.#changeSoundVolume();

        //remove all timeouts
        for (let soundName in this.#activeTimeouts) {
            clearTimeout(this.#activeTimeouts[soundName]);
            delete this.#activeTimeouts[soundName];
        }
    }

    SetGamePlayContinued() {
        this.#gamePlayPaused = false;
        this.#changeSoundVolume();
    }

    ChangeVolume(volume) {
        Howler.volume(volume);
    }


    #changeSoundVolume() {
        if (this.#gamePlayPaused) {
            Howler.mute(true);
        } else {
            Howler.mute(false)
        }
    }
}
