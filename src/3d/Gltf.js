import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

const loader = new GLTFLoader();

export class GLTF {
    _filename;
    _gltf;


    _z = 0;
    _y = 0;

    _castShadow = false;
    _receiveShadow = false;


    async Load() {
        if (!this._filename) {
            throw new Error("Please provide gltf filename");
        }

        this._gltf = await loader.loadAsync(`3d/${this._filename}`);


        this._gltf.scene.position.z = this._z;
        this._gltf.scene.position.y = this._y;

        this._gltf.scene.traverse(child => {
            if (child.isMesh) {
                child.castShadow = this._castShadow;
                child.receiveShadow = this._receiveShadow;
                child.material.shadowSide = THREE.FrontSide;

            }
        });
    }

    get scene() {
        return this._gltf.scene;
    }

    get Position() {
        return this._gltf.scene.position
    }

    get Quaternion() {
        return this._gltf.scene.quaternion
    }
} 