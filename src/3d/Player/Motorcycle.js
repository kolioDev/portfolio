import { Vehicle } from "./Vehicle";


export class Motorcycle extends Vehicle {

    _SPEED_ANIMATION_MULTIPLYER = 5 / 100;
    _VELOCITY_MULTIPLYER = 1 / 40000;
    _MAX_SPEED = 120;
    _MIN_SPEED = -50;
    _ACCELERATION_FORWARD = .3;
    _DECELERATION = -.07;
    _ACCELERATION_BACKWARDS = -.05;

    _filename = 'motorcycle.glb';

    async Load() {
        await super.Load();

        // Play all animations
        this._gltf.animations.forEach((clip) => {
            this._animationMixer.clipAction(clip).play();
        });

    
    }
}