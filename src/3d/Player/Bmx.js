import { Vehicle } from "./Vehicle";
import * as THREE from 'three';

const WIGGLE = 'Wiggle';
const JUMP = 'Jump';
const PEDAL = 'Pedal';
const DROP = 'Drop';

const DROP_SOUND = 'drop'

const THROTTLE_BACKWARD = 'THROTTLE_BACKWARD';
const THROTTLE_FORWARD = 'THROTTLE_FORWARD';
const THROTTLE_RELEASE = 'THROTTLE_RELEASE';

export class BMX extends Vehicle {

    _SPEED_ANIMATION_MULTIPLYER = 3.7 / 100;
    _VELOCITY_MULTIPLYER = 1 / 40000;
    _MAX_SPEED = 115;
    _MIN_SPEED = -100;
    _ACCELERATION_FORWARD = .3;
    _DECELERATION = -.07;
    _ACCELERATION_BACKWARDS = -.05;
    _DROP_ANIMATION_MULTIPLYER = 2.5;
    _MIN_SPEED_FOR_MOVING_SOUND = 25;

    _filename = 'bmx.glb'
    _movingSoundPath = "sounds/wheel_spin.mp3";

    #ANIMATION_PERIOD = 130; //frames
    #JUMP_PRROBABILITY = .4;

    #DROP_ANIMATION_DELAY = 13 //frames

    #animationActions = {};
    #framesFromLastAnimation = 0;
    #delayedDropFrames = 0;
    #animationInProgress = false;

    #throttleToExecuteAfterAnimation = null;


    async Load() {
        await super.Load();


        this._gltf.scene.traverse(function (child) {
            if (child.isMesh) {
                child.castShadow = true;
                child.receiveShadow = true;
            }
        });


        // Clip all animations
        this._gltf.animations.forEach((clip) => {
            this.#animationActions[clip.name] = this._animationMixer.clipAction(clip);
        });

        this.#animationActions[PEDAL].play();

        this.#animationActions[JUMP].setLoop(THREE.LoopOnce);
        this.#animationActions[WIGGLE].setLoop(THREE.LoopOnce);
        this.#animationActions[DROP].setLoop(THREE.LoopOnce);
        this.#animationActions[DROP].zeroSlopeAtStart = false;

        this._animationMixer.addEventListener('finished', (e) => {
            this.#animationFinished(e.action.getClip().name)
        })

        //register sounds
        this._soundManager.RegisterSound(DROP_SOUND, 'sounds/bike_drop.mp3');

    }

    Animate(deltaT) {

        this.#animationLoopPlayFunnyAnimations()
        this.#animationLoopPlayDropAnimation()
     
        super.Animate(deltaT);

    }


    ThrottleForward() {
        if (this.#animationInProgress) {
            this.#throttleToExecuteAfterAnimation = THROTTLE_FORWARD;
            return;
        }
        super.ThrottleForward();
    }

    ThrottleBackward() {
        if (this.#animationInProgress) {
            this.#throttleToExecuteAfterAnimation = THROTTLE_BACKWARD;
            return;
        }
        super.ThrottleBackward();
    }

    ThrottleRelease() {
        if (this.#animationInProgress) {
            this.#throttleToExecuteAfterAnimation = THROTTLE_RELEASE;
            return;
        }

        super.ThrottleRelease();
    }

    DropToPosition(x) {
        super.DropToPosition(x);
        this.#delayedDropFrames = 0;
        this.#animationActions[PEDAL].stop();
        this.#cancelAnimations();

        this.#animationActions[DROP].play();

    }

    #animationFinished(animationName) {
        if (animationName === DROP) {
            this._isDroppingToPosition = false;
            this.#animationActions[PEDAL].play();
        }

        this.#cancelAnimations()

        if (this.#throttleToExecuteAfterAnimation) {
            switch (this.#throttleToExecuteAfterAnimation) {
                case THROTTLE_RELEASE:
                    this.ThrottleRelease();
                    break;
                case THROTTLE_FORWARD:
                    this.ThrottleForward();
                    break;
                case THROTTLE_BACKWARD:
                    this.ThrottleBackward();
                    break;
            }
        }

        this.#throttleToExecuteAfterAnimation = null;
    }

    #cancelAnimations() {
        this.#animationInProgress = false;
        this.#animationActions[JUMP].reset();
        this.#animationActions[JUMP].stop();
        this.#animationActions[WIGGLE].reset();
        this.#animationActions[WIGGLE].stop();
        this.#animationActions[DROP].reset();
        this.#animationActions[DROP].stop();
    }


    #animationLoopPlayFunnyAnimations() {
        this.#framesFromLastAnimation++;

        //Logic for playing Wiggle/Jump animations
        if (
            this._speed === this._MAX_SPEED &&
            this.#framesFromLastAnimation > this.#ANIMATION_PERIOD &&
            !this.#animationInProgress
        ) {
            this.#animationInProgress = true;
            let a = Math.random() > this.#JUMP_PRROBABILITY ? WIGGLE : JUMP;

            this.#animationActions[a].play();
            this.#framesFromLastAnimation = 0;

            if(a === JUMP) {
                this._soundManager.PlaySoundDelayed(DROP_SOUND, 650);
            }
        }

        if (this._speed == 0) {
            this.#framesFromLastAnimation = 0;
            this.#animationInProgress = false;
        }
    }

    #animationLoopPlayDropAnimation() {
        //Logic for playing Drop animation (with delay a few frames)
        if (this._isDroppingToPosition) {
            this.#delayedDropFrames ++;
            if (this.#delayedDropFrames === this.#DROP_ANIMATION_DELAY) {
                this.#animationActions[DROP].paused = false;
                this._gltf.scene.position.y = 0;
                this.#delayedDropFrames = -Infinity;

                this._soundManager.PlaySoundDelayed(DROP_SOUND, [130, 950, 200]);
            }
            // Lets 3 frames of animation to finish before we start dropping
            // This ensures the bike is in the first frame needed
            else if (this.#delayedDropFrames === 3) {
                this.#animationActions[DROP].paused = true;
            }
        }
    }

}