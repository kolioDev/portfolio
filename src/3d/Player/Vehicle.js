import * as THREE from 'three';
import _ from 'lodash';

import { SoundManager } from '../Managers/SoundManager.js';
import { GLTF } from '../Gltf.js';

const MOVING_SOUND = 'moving';

//Base vehicle class
export class Vehicle extends GLTF {

    _SPEED_ANIMATION_MULTIPLYER;
    _VELOCITY_MULTIPLYER;
    _MAX_SPEED;
    _MIN_SPEED;
    _ACCELERATION_FORWARD;
    _DECELERATION;
    _ACCELERATION_BACKWARDS;
    _MIN_SPEED_FOR_MOVING_SOUND;

    _gltf;
    _filename;
    _movingSoundPath;;

    _animationMixer;
    _speed = 0;
    _accelerating = 0 //-1,0,1

    _isDroppingToPosition = false;

    _soundManager;


    constructor(params) {
        super(params);
        this._soundManager = new SoundManager();


    }

    async Load() {
        await super.Load();

        this._gltf.scene.rotation.y = Math.PI / 2;

        this._animationMixer = new THREE.AnimationMixer(this._gltf.scene);

        if (this._movingSoundPath) {
            this._soundManager.RegisterSound(MOVING_SOUND, this._movingSoundPath, {
                repeat: true,
                volume: 0.6
            });
        }

    }

    Animate(deltaT) {
        this._calculateSpeed(deltaT);

        this._animationMixer.timeScale = this._animationTimeScale();

        this._animationMixer.update(deltaT / 1000); //time is in seconds

        this._gltf.scene.position.x += this._velocityFactor(deltaT);

        if (Math.abs(this._speed) > this._MIN_SPEED_FOR_MOVING_SOUND) {
            this._soundManager.PlaySound(MOVING_SOUND);
            this._soundManager.SetRate(MOVING_SOUND, Math.abs(this._speed) / this._MAX_SPEED);
        } else {
            this._soundManager.PauseSound(MOVING_SOUND);
        }
    }

    _calculateSpeed(deltaT) {
        const roundRange = 1;
        const roundIfClose = (speed, value) => {
            if (_.inRange(speed, value - roundRange, value + roundRange)) {
                speed = value;
            }
            return speed;
        }

        let a = 0;
        if (this._accelerating === 1 && this._speed < this._MAX_SPEED) {
            a = this._ACCELERATION_FORWARD
        } else if (this._accelerating === -1 && this._speed > this._MIN_SPEED) {
            a = this._speed > 0 ? -this._ACCELERATION_FORWARD : this._ACCELERATION_BACKWARDS
        } else if (this._accelerating === 0 && this._speed != 0) {
            a = this._speed > 0 ? this._DECELERATION : -this._DECELERATION;
        }

        this._speed = this._speed + a * deltaT;

        this._speed = roundIfClose(this._speed, 0);
        this._speed = roundIfClose(this._speed, this._MAX_SPEED);
        this._speed = roundIfClose(this._speed, this._MIN_SPEED);

        this._speed = _.clamp(this._speed, this._MIN_SPEED, this._MAX_SPEED);
    }

    _animationTimeScale() {
        if (this._isDroppingToPosition) {
            return this._DROP_ANIMATION_MULTIPLYER;
        }
        return this._speed * this._SPEED_ANIMATION_MULTIPLYER
    }

    _velocityFactor(deltaT) {
        return this._speed * deltaT * this._VELOCITY_MULTIPLYER;
    }


    get scene() {
        return this._gltf.scene;
    }

    get speed() {
        return this._speed;
    }


    get position() {
        return this._gltf?.scene?.position;
    }

    get deceleration() {
        return this._DECELERATION
    }

    get velocityMultiplier() {
        return this._VELOCITY_MULTIPLYER;
    }

    get isDroppingToPosition() {
        return this._isDroppingToPosition;
    }

    ThrottleForward() {
        if (!this._isDroppingToPosition)
            this._accelerating = 1;
    }

    ThrottleBackward() {
        if (!this._isDroppingToPosition)
            this._accelerating = -1;
    }

    ThrottleRelease() {
        this._accelerating = 0;
    }

    DropToPosition(x) {
        this._speed = 0;
        this._accelerating = 0;

        this._isDroppingToPosition = true;

        this._gltf.scene.position.x = x;
    }
}