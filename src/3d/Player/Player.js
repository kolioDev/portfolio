import { ClickHandler } from "../Managers/ClickHandler";
import { BMX } from "./Bmx";
// import { Motorcycle } from "./Motorcycle";
import { PLAYERS } from "./players";
export class Player{
    #playerObject=new BMX() //new Motorcycle(); ;// new Motorcycle();
    #clickHandler;

    async Load(){
        await this.vehicle.Load();
    }

    AttachClickHandler(){
        this.#clickHandler = new ClickHandler();
        this.#clickHandler.AddObject({
            object:this.vehicle.scene,
            key:PLAYERS.bmx
        })
    }

    Animate(deltaT){
        this.#playerObject.Animate(deltaT);
    }

    get vehicle(){
        return this.#playerObject
    }
}