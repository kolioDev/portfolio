import * as THREE from 'three';

import { ThirdPersonCamera } from './ThirdPersonCamera';

export class Camera {

    camera

    #sceneWidth
    #sceneHeight
    #thirdPersonCamera
    #target

    constructor({ sceneWidth, sceneHeight, target, player}) {
        this.#sceneWidth = sceneWidth;
        this.#sceneHeight = sceneHeight;
        this.#target = target;

        this.camera = new THREE.PerspectiveCamera(50,
            this.#sceneWidth / this.#sceneHeight,
            0.1, 50);

        this.#thirdPersonCamera = new ThirdPersonCamera({
            camera: this.camera,
            target: this.#target,
            player
        });
    }

    Animate(deltaT) {
        this.#thirdPersonCamera.Animate(deltaT);
    }

    Resize({ sceneWidth, sceneHeight }) {
        this.#sceneWidth = sceneWidth;
        this.#sceneHeight = sceneHeight;

        this.camera.aspect = sceneWidth / sceneHeight
        this.camera.updateProjectionMatrix();
    }

    Tilt(tilt) {
        this.#thirdPersonCamera.Tilt(tilt)
    }

    get fieldOfView() {
        return this.#thirdPersonCamera.fieldOfView
    }

}