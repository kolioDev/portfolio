import * as THREE from 'three';
import _ from 'lodash';

//Do not touch here if you want to increase the render span ! 
//(2 painful days of trial and errors)
const FOV_B_L = [-2, -2];
const FOV_T_R = [2, 2];

//How greater the fov used for the conditional rendering is compared to the actual camera fov
const FOV_MULTIPLE = 11;
const FOV_TRANSLATE_X = 6.54;

const TILT_VERTICAL_AMOUNT = .6;
const TILT_HORIZONTAL_AMOUNT = 1;

const LERP_FACTOR = 0.00312;

export class ThirdPersonCamera {

    #camera;
    #target;
    #player;

    #tilt = {
        x: .5,
        y: .5
    }; //[0;1]

    #currentPosition;
    #currentLookat;

    #fov;

    #offsetCalculationVector;
    #lookatCalculationVector;

    constructor({ camera, target, player }) {
        this.#camera = camera;
        this.#target = target;
        this.#player = player;

        this.#currentPosition = new THREE.Vector3();
        this.#currentLookat = new THREE.Vector3();

        this.#fov = new THREE.Box3(
            new THREE.Vector2(...FOV_B_L),
            new THREE.Vector2(...FOV_T_R),
        );

        this.#offsetCalculationVector = new THREE.Vector3();
        this.#lookatCalculationVector = new THREE.Vector3();
    }

    #CalculateIdealOffset() {
        const tiltV = - this.#tilt.y * TILT_VERTICAL_AMOUNT;
        const tiltX = -this.#tilt.x * TILT_HORIZONTAL_AMOUNT;

        this.#offsetCalculationVector.set(0.8 + tiltX, 4, 7)
        this.#offsetCalculationVector.add(this.#target.Position);
        this.#offsetCalculationVector.y = 4 + tiltV
        return this.#offsetCalculationVector;
    }

    #CalculateIdealLookat() {
        this.#lookatCalculationVector.set(0,2,0);
        this.#lookatCalculationVector.applyQuaternion(this.#target.Quaternion);
        this.#lookatCalculationVector.add(this.#target.Position);
        this.#lookatCalculationVector.y = 2;
        return this.#lookatCalculationVector;
    }

    Animate(deltaT) {
        const idealOffset = this.#CalculateIdealOffset();
        const idealLookat = this.#CalculateIdealLookat();

        const t = deltaT * LERP_FACTOR;

        if (this.#lerpEnabled() && deltaT !== 0) {
            this.#currentPosition.lerp(idealOffset, t);
            this.#currentLookat.lerp(idealLookat, t);
        } else {
            this.#currentLookat = idealLookat;
            this.#currentPosition = idealOffset;
        }

        this.#camera.position.copy(this.#currentPosition);
        this.#camera.lookAt(this.#currentLookat);
    }

    Tilt(tilt) {
        this.#tilt = tilt
    }

    #lerpEnabled() {
        return !this.#player.vehicle.isDroppingToPosition
    }

    get fieldOfView() {
        const aspect = this.#camera.aspect;

        this.#fov.min.x = FOV_B_L[0] * aspect;
        this.#fov.max.x = FOV_T_R[0] * aspect;
        this.#fov.min.y = FOV_B_L[1] / aspect;
        this.#fov.max.y = FOV_T_R[1] / aspect;

        this.#fov.translate(this.#camera.position);
        this.#fov.expandByScalar(FOV_MULTIPLE);

        //Translate in X direction.
        this.#fov.min.x = this.#fov.min.x + FOV_TRANSLATE_X
        this.#fov.max.x = this.#fov.max.x + FOV_TRANSLATE_X


        return this.#fov;
    }


}



