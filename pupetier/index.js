const puppeteer = require('puppeteer');


const HEADLESS = false;
const OSIRIS_URL = "https://osiris.tue.nl"

const  main =  async () => {
  const browser = await puppeteer.launch({ headless: HEADLESS });
  const page = await browser.newPage();

  await page.goto(OSIRIS_URL);

  await browser.close();
};


main()